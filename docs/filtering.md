[< Go to index](../README.md)

## Filtros

### Advance Eloquent Query Constraints
En este episodio vamos a ver posibilidades avanzadas de filtros con eloquent

Modificamos el `scopeFilter()` del modelo `Post` el filtro de categorias, hacemos uso de `where`, `orWhere` y `whereHas`
```php
public function scopeFilter($query, array $filters){
    $query->when($filters["search"] ?? false, fn($query, $search) =>
        $query->where('title', 'like', '%'.$search.'%')
                ->orWhere('body', 'like', '%'.$search.'%'));

    $query->when($filters["category"] ?? false, fn($query, $category) =>
        $query->whereHas('category', fn ($query) =>
            $query->where('slug',$category)
        ));
}
```

Modificamos metodo index de PostController para combinar busquedas de categorias y search
```php
public function index(){
    return view('posts',[
        'posts'=> Post::latest()->filter(request(['search', 'category']))->get(),
        'categories'=> Category::all(),
        'currentCategory' => Category::firstWhere('slug',request('category'))
    ]);
}
```

En **_post-header** modificamos esta seccion para filtro de categorias
```html
@foreach ($categories as $category)
    <x-dropdown-item
        href="/?category={{ $category->slug }}"
        :active='request()->is("categories/{$category->slug}")'
    >{{ ucwords($category->name) }}</x-dropdown-item>
@endforeach
```

Ahora limpiamos archivo de **routes** y queda asi
```php
<?php

use App\Http\Controllers\PostController;
use App\Models\Post;
use App\Models\Category;
use App\Models\User;
use Illuminate\Support\Facades\Route;
use Spatie\YamlFrontMatter\YamlFrontMatter;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'index'])->name("home");

Route::get('/posts/{post:slug}',[PostController::class, 'show']);


Route::get('authors/{author:username}',function(User $author){
    return view('posts',[
        'posts' => $author->posts,
        'categories'=> Category::all()
    ]);
});

```

---

### Extract a Category Dropdown Blade Component
Vamos a mejorar el dropdown creando un componente de dropdown

En la consola ejecutamos
```console
php artisan make:component CategoryDropdown
```
**app/View/Components/CategoryDropdown**
```php
<?php

namespace App\View\Components;

use App\Models\Category;
use Illuminate\View\Component;

class CategoryDropdown extends Component
{
    /**
     * Get the view / contents that represent the component.
     *
     */
    public function render()
    {
        return view('components.category-dropdown',[
            'categories' => Category::all(),
            'currentCategory' => Category::firstWhere('slug',request('category'))
        ]);
    }
}

```
Tambien se creó el archivo blade **category-dropdown** que va a contener los dropdown items
```php
<x-dropdown>
    <x-slot name="trigger">
        <button class="py-2 pl-3 pr-9 text-sm font-semibold lg:w-32 text-left flex lg:inline-flex">
            {{ isset($currentCategory) ? ucwords($currentCategory->name) : 'Categories' }}
            <x-icon name="down-arrow" class="absolute pointer-events-none" style="right:12px" />
        </button>
    </x-slot>

    <x-dropdown-item href="/" :active="request()->routeIs('home')">All</x-dropdown-item>
    @foreach ($categories as $category)
        <x-dropdown-item
            href="/?category={{ $category->slug }}"
            :active='request()->is("categories/{$category->slug}")'
        >{{ ucwords($category->name) }}</x-dropdown-item>
    @endforeach
</x-dropdown>

```
Vamos a organizar mejor las vistas, por lo que creamos una carpeta `posts` dentro de `views` y movemos los archivos que ademas vamos a renombrar para seguir estandar de archivos, movemos `posts` y se renombra a `index`, `_posts-header` se renombra a `_header` y `post` se renombra a `show`

Archivo **posts/_header** queda de la siguiente forma
```php
<header class="max-w-xl mx-auto mt-20 text-center">
    <h1 class="text-4xl">
        Latest <span class="text-blue-500">Laravel From Scratch</span> News
    </h1>


    <div class="space-y-2 lg:space-y-0 lg:space-x-4 mt-4">
        <!--  Category -->
        <div class="relative lg:inline-flex bg-gray-100 rounded-xl">
            <x-category-dropdown />
        </div>

    
        <!-- Search -->
        <div class="relative flex lg:inline-flex items-center bg-gray-100 rounded-xl px-3 py-2">
            <form method="GET" action="#">
                <input type="text" name="search" placeholder="Find something"
                       class="bg-transparent placeholder-black font-semibold text-sm"
                       value="{{ request('search') }}">
            </form>
        </div>
    </div>
</header>
```

Archivo **posts/index**
```php
<x-layout>
    @include('posts._header')

    <main class="max-w-6xl mx-auto mt-6 lg:mt-20 space-y-6">
        @if ($posts->count()>0)
            <x-posts-grid :posts="$posts" />
        @else
            <p class="text-center">No posts yet. Please check back later</p>
        @endif
    </main>

</x-layout>

```
Y actualizamos **PostController** llamando las vistas con el estandar
```php
<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(){
        return view('posts.index',[
            'posts'=> Post::latest()->filter(request(['search', 'category']))->get()
        ]);
    }

    public function show(Post $post) {
        return view('posts.show',[
            'post' => $post
        ]);
    }
}

```

---

### Author Filtering
Vamos a crear filtrado de authors


Agregamos enlace donde esta el nombre del author a las vistas de **post-card, post-featured-card y show**
```html
<h5 class="font-bold">
    <a href="/?author={{ $post->author->username }}">{{ $post->author->name }}</a>
</h5>
```
Agregamos al **PostController** metodo index el filtro de author
```php
public function index(){
    return view('posts.index',[
        'posts'=> Post::latest()->filter(request(['search', 'category','author']))->get()
    ]);
}
```
Modificamos scopreFilter() del modelo **Post.php**
```php
public function scopeFilter($query, array $filters){
    $query->when($filters["search"] ?? false, fn($query, $search) =>
        $query->where('title', 'like', '%'.$search.'%')
                ->orWhere('body', 'like', '%'.$search.'%'));

    $query->when($filters["category"] ?? false, fn($query, $category) =>
        $query->whereHas('category', fn ($query) =>
            $query->where('slug',$category)
        ));

    $query->when($filters["author"] ?? false, fn($query, $author) =>
        $query->whereHas('author', fn ($query) =>
            $query->where('username',$author)
        ));
}
```

Y ahora podemos tener todo funcionando solamente con 2 rutas, el archivo **routes** queda mas limpio
```php
<?php

use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;


Route::get('/', [PostController::class, 'index'])->name("home");

Route::get('/posts/{post:slug}',[PostController::class, 'show']);


```

---

### Merge Category and Search Queries

Ahora lo que vamos a hacer es integrar ambos filtros de search y categories para que se unan en el query string/parametros del URL

Modificamos formulario **_header** agregando condicion para el parametro category
```html
<header class="max-w-xl mx-auto mt-20 text-center">
    <h1 class="text-4xl">
        Latest <span class="text-blue-500">Laravel From Scratch</span> News
    </h1>


    <div class="space-y-2 lg:space-y-0 lg:space-x-4 mt-4">
        <!--  Category -->
        <div class="relative lg:inline-flex bg-gray-100 rounded-xl">
            <x-category-dropdown />
        </div>

        <!-- Search -->
        <div class="relative flex lg:inline-flex items-center bg-gray-100 rounded-xl px-3 py-2">
            <form method="GET" action="#">
                @if (request('category'))
                    <input type="hidden" name="category" value="{{ request('category') }}">
                @endif
                <input type="text" name="search" placeholder="Find something"
                       class="bg-transparent placeholder-black font-semibold text-sm"
                       value="{{ request('search') }}">
            </form>
        </div>
    </div>
</header>

```
Y modificamos componente **category-dropdown** concatenando url de search
```html
<x-dropdown>
    <x-slot name="trigger">
        <button class="py-2 pl-3 pr-9 text-sm font-semibold lg:w-32 text-left flex lg:inline-flex">
            {{ isset($currentCategory) ? ucwords($currentCategory->name) : 'Categories' }}
            <x-icon name="down-arrow" class="absolute pointer-events-none" style="right:12px" />
        </button>
    </x-slot>

    <x-dropdown-item href="/" :active="request()->routeIs('home')">All</x-dropdown-item>
    @foreach ($categories as $category)
        <x-dropdown-item
            href="/?category={{ $category->slug }}&{{ http_build_query(request()->except('category')) }}"
            :active='request()->is("categories/{$category->slug}")'
        >{{ ucwords($category->name) }}</x-dropdown-item>
    @endforeach
</x-dropdown>

```
---

### Fix a confusing Eloquent Query Bug
Corregir bug donde estamos obteniendo resultados erroneos en los filtros ya que la consulta final que esta creando Eloquent nos da el query no exactamente con las condiciones que queriamos

La solución es modificar scopeFilter de modelo **Post** en el filtro de search
```php
public function scopeFilter($query, array $filters){
    $query->when($filters["search"] ?? false, fn($query, $search) =>
        $query->where(fn($query) =>
                $query->where('title', 'like', '%'.$search.'%')
                ->orWhere('body', 'like', '%'.$search.'%'))
        );

    $query->when($filters["category"] ?? false, fn($query, $category) =>
        $query->whereHas('category', fn ($query) =>
            $query->where('slug',$category)
        ));

    $query->when($filters["author"] ?? false, fn($query, $author) =>
        $query->whereHas('author', fn ($query) =>
            $query->where('username',$author)
        ));
}
```
---



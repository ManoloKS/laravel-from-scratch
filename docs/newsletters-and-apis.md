[< Go to index](../README.md)

## Newsletters y APIs

### Mailchimp API Tinkering
Vamos a utilizar el boton de subscribe para suscribir usuarios a mailchimp usando el api de mailchimp

Agregamos id al footer del `layout`
```html
<footer id="newsletter" class="bg-gray-100 border border-black border-opacity-5 rounded-xl text-center py-16 px-10 mt-16">
```

Agregamos ancla hacia el footer en el boton de subscribe
```html
<a href="#newsletter" class="bg-blue-500 ml-3 rounded-full text-xs font-semibold text-white uppercase py-3 px-5">
    Subscribe for Updates
</a>
```

Creamos una cuenta en mailchimp, y en account->APIS, creamos una cuenta, esto nos dara un API Key que debemos copiar en nuestro archivo **.env**
```php
MAILCHIMP_KEY="19a6cc944c6f2439438f1575bd6b1aa9-us5"
```

Ahora agregamos la referencia de mailchimp key a la config de laravel en el archivo **/config/services.php**
```php
'mailchimp' => [
    'key' => env('MAILCHIMP_KEY')
]

```

Instalamos paquete de mailchimp con composer en el proyecto
```console
composer require mailchimp/marketing
```


Ahora hacemos pruebas con el API desde **routes**
```php
Route::get('ping', function () {
    $mailchimp = new \MailchimpMarketing\ApiClient();
    $mailchimp->setConfig([
        'apiKey' => config('services.mailchimp.key'),
        'server' => 'us5'
    ]);

    $response = $mailchimp->ping->get();
    print_r($response);
});

Route::get('ping', function () {
    $mailchimp = new \MailchimpMarketing\ApiClient();
    $mailchimp->setConfig([
        'apiKey' => config('services.mailchimp.key'),
        'server' => 'us5'
    ]);

    $response = $mailchimp->lists->getList('590069b95c');
    dd($response);
});
```

Prueba para agregar a alguien a la lista
```php
Route::get('ping', function () {
    $mailchimp = new \MailchimpMarketing\ApiClient();
    $mailchimp->setConfig([
        'apiKey' => config('services.mailchimp.key'),
        'server' => 'us5'
    ]);

    $response = $mailchimp->lists->addListMember('590069b95c',[
        'email_address' => 'pokedexter2003@gmail.com',
        'status' => 'subscribed'
    ]);
    dd($response);
});
```

---

### Make the Newsletter Form Work

Ahora que ya tenemos el API de mailchimp funcionando con pruebas, vamos a ponerlo a funcionar con el formulario de subscribe

Agregamos la ruta y con su funcion de validacion a **routes**
```php
Route::post('newsletter', function () {
    request()->validate(['email'=>'required|email']);
    $mailchimp = new \MailchimpMarketing\ApiClient();
    $mailchimp->setConfig([
        'apiKey' => config('services.mailchimp.key'),
        'server' => 'us5'
    ]);

    try{
        $response = $mailchimp->lists->addListMember('590069b95c',[
            'email_address' => request('email'),
            'status' => 'subscribed'
        ]);
    }catch(\Exception $e){
        throw \Illuminate\Validation\ValidationException::withMessages([
            'email' => 'This email could not be added to our newsletter list'
        ]);
    }
    return redirect('/')->with('success', 'You are now signed up for our newsletter');
});
```

Modificamos ruta del form en **layout** y agregamos mensaje de error
```html
<form method="POST" action="/newsletter" class="lg:flex text-sm">
    @csrf
    <div class="lg:py-3 lg:px-5 flex items-center">
        <label for="email" class="hidden lg:inline-block">
            <img src="/images/mailbox-icon.svg" alt="mailbox letter">
        </label>

        <div>
            <input id="email" name="email" type="text" placeholder="Your email address"
                class="lg:bg-transparent py-2 lg:py-0 pl-4 focus-within:outline-none">
            @error('email')
                <span class="text-xs text-red-500">{{ $message }}</span>
            @enderror
        </div>
    </div>

    <button type="submit"
            class="transition-colors duration-300 bg-blue-500 hover:bg-blue-600 mt-4 lg:mt-0 lg:ml-3 rounded-full text-xs font-semibold text-white uppercase py-3 px-8"
    >
        Subscribe
    </button>
</form>
```

---


### Extract a Newsletter Service

Vamos a organizar la funcionabilidad de el servicio del API del newsletter

Primero creamos directorio **Services** dentro de la carpeta **app** con archivo **Newsletter.php**
**app/Services/Newsletter.php** este archivo tendra la conexion con el API a una lista en especifico
```php
<?php

namespace App\Services;

use MailchimpMarketing\ApiClient;

class Newsletter{

    public function subscribe(string $email, string $list = null){

        $list ??= config('services.mailchimp.lists.subscribers');

        return $this->client()->lists->addListMember($list,[
            'email_address' => request('email'),
            'status' => 'subscribed'
        ]);
    }

    protected function client(){
        return (new ApiClient())->setConfig([
            'apiKey' => config('services.mailchimp.key'),
            'server' => 'us5'
        ]);
    }
}

```

Agregamos la lista que tenemos a archivo **.env**
```php
MAILCHIMP_LIST_SUBSCRIBERS="590069b95c"
```

Agregamos nueva configuracion a **config/services.php**
```php
'mailchimp' => [
    'key' => env('MAILCHIMP_KEY'),
    'lists' => [
        'subscribers' => env('MAILCHIMP_LIST_SUBSCRIBERS')
    ]
]
```

Creamos nuevo controlador **NewsletterController**
```console
php artisan make:controller NewsletterController
```

Agregamos lógica de  agregar email al newsletter en el nuevo controller
```php
<?php

namespace App\Http\Controllers;

use App\Services\Newsletter;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    public function __invoke(Newsletter $newsletter)
    {
        request()->validate(['email'=>'required|email']);


        try{
            $newsletter->subscribe(request('email'));
        }catch(\Exception $e){
            throw \Illuminate\Validation\ValidationException::withMessages([
                'email' => 'This email could not be added to our newsletter list'
            ]);
        }

        return redirect('/')->with('success', 'You are now signed up for our newsletter');
    }
}

```

Finalmente agregamos nuestra ruta newsletter
```php
Route::post('newsletter', NewsletterController::class);
```

---

### Toy Chests and Contracts

En este capitulo nos explican como laravel hace binding automatico con un ejemplo de service container como un contenedor de juguetes, ademas vamos a hacer os newsletter mas flexibles en caso de que se necesite otro servicio aparte que no sea mailchimp

Vamos a hacer una interfaz para poder utilizar diferentes servicios de API ademas de mailchimp

Cambiamos nombre de **Services/Newsletter** a **Services/MailchimpNewsLetter.php** y ademas creamos un nuevo servicio de prueba **Services/ConvertKitNewsletter**

Creamos nueva interfaz dentro de Services, **/app/Services/Newsletter.php**

**Newsletter.php**
```php
<?php

namespace App\Services;

interface Newsletter{
    public function subscribe(string $email, string $list = null);
}

```

**Services/MailchimpNewsLetter.php**

```php
<?php

namespace App\Services;

use MailchimpMarketing\ApiClient;

class MailchimpNewsletter implements Newsletter{

    protected ApiClient $client;
    public function __construct(ApiClient $client)
    {
        $this->client = $client;
    }

    public function subscribe(string $email, string $list = null){

        $list ??= config('services.mailchimp.lists.subscribers');

        return $this->client->lists->addListMember($list,[
            'email_address' => $email,
            'status' => 'subscribed'
        ]);
    }
}

```

**Services/ConvertKitNewsletter**
```php
<?php

namespace App\Services;


class ConvertKitNewsletter implements Newsletter{

    public function subscribe(string $email, string $list = null){

        //subscribe the user with ConvertKit-specific
        //API request
    }
}

```
Modificamos **NewsletterController**
```php
<?php

namespace App\Http\Controllers;

use App\Services\Newsletter;
use Illuminate\Validation\ValidationException;

class NewsletterController extends Controller
{
    public function __invoke(Newsletter $newsletter)
    {
        request()->validate(['email'=>'required|email']);

        try{
            $newsletter->subscribe(request('email'));
        }catch(\Exception $e){
            throw ValidationException::withMessages([
                'email' => 'This email could not be added to our newsletter list'
            ]);
        }

        return redirect('/')->with('success', 'You are now signed up for our newsletter');
    }
}

```

Ahora agregamos referencia al binding en el archivo **app/Providers/AppServiceProvider.php**
```php
public function register()
{
    app()->bind(Newsletter::class, function(){
        $client = (new ApiClient)->setConfig([
            'apiKey' => config('services.mailchimp.key'),
            'server' => 'us5'
        ]);
        return new MailchimpNewsletter($client);
    });
}
```
De esta forma podemos hacer uso de los service containers y tener diferentes servicios de API de un mismo tipo

---

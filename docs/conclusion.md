[< Go to index](../README.md)

## Conclusion
---

### Goodbye and Next Steps

Ya terminamos el curso y ahora JeffreyWay nos contribuye con un enlace de github para tener el proyecto y una guia para la instalación desde 0, ademas que nos da futuras ideas para desarrollar a futuro al proyecto

Link: https://github.com/JeffreyWay/Laravel-From-Scratch-Blog-Project/blob/main/README.md

Nos menciona ademas que otras cosas podemos aprender después, como: **Queues, Events, Compiling Assets(Mix), Eloquent Relationships, Custom artisan commands, HTTP Tests, Notifications, API Resources

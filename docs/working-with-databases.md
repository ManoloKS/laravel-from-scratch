[< Go to index](../README.md)

## Working with Databases

### Environment Files and Database Connections
Crear y configurar base de datos para el uso del curso
```console
sudo mysql
create database lfs;
create user lfs identified by 'secret';
grant all privileges on lfs.* to lfs;
flush privileges;
```

Configurar archivo `.env` con la configuración de la base de datos
```php
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=lfs
DB_USERNAME=lfs
DB_PASSWORD=secret
```
Correr comando para crear primera migracion desde el webserver
```console
php artisan migrate
```
---
### Migrations: The Absolute Basics
En los archivos de `/database/migrations` podemos encontrar las migraciones, los archivos se componen basicamente de 2 metodos, `up y down`, `up` es para correr la migracion y `down` para revertir 

Hacemos cambios en la migracion de users modificando `name` a `username` y agregando `is_admin`
```php
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username');
            $table->string('email')->unique();
            $table->boolean('is_admin')->default(false);
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
```

Para aplicar los cambios primero hay que revertir la primera migracion
```console
php artisan migrate:rollback
```
Despues ejecutar nuevamente 
```console
php artisan migrate
```
Comando para borrar todas las tablas y empezar de 0

```console
php artisan migrate:fresh
```
---

### Eloquent and the Active Record Pattern 
Regresar migracion de users a como estaba por default

```php
    Schema::create('users', function (Blueprint $table) {
        $table->id();
        $table->string('name');
        $table->string('email')->unique();
        $table->timestamp('email_verified_at')->nullable();
        $table->string('password');
        $table->rememberToken();
        $table->timestamps();
    });
```
Correr comando en webserver
```console
php artisan migrate:fresh
```
Probando Eloquent desde Consola
```console
php artisan tinker
Psy Shell v0.10.8 (PHP 7.4.21 — cli) by Justin Hileman
>>> User::all();
[!] Aliasing 'User' to 'App\Models\User' for this Tinker session.
=> Illuminate\Database\Eloquent\Collection {#4290
     all: [],
   }
>>> $user = new User;
=> App\Models\User {#3496}
>>> $user->name = "JohnDoe"
=> "JohnDoe"
>>> $user->email = "johndoe@email.com"
=> "johndoe@email.com"
>>> $user->password = bcrypt('secret');
=> "$2y$10$Y3GA.p4aM5LYqR.mCXLeCOnZFyn7jxX7vvSZ1RA2Mqujnvay1eSF2"
>>> $user->save();
=> true
>>> $user = new User();
=> App\Models\User {#4380}
>>> $user->name = "Sally"
=> "Sally"
>>> $user->email = 'sally@email.com';
=> "sally@email.com"
>>> $user->password = bcrypt('sally!');
=> "$2y$10$LNQiHPxYRgVMq70ehswdjuMcXS2R435SknLIpLxONbrcNtZdIw9zS"
>>> $user->save();
=> true
>>> User::all();
=> Illuminate\Database\Eloquent\Collection {#4435
     all: [
       App\Models\User {#4222
         id: 1,
         name: "JohnDoe",
         email: "johndoe@email.com",
         email_verified_at: null,
         #password: "$2y$10$Y3GA.p4aM5LYqR.mCXLeCOnZFyn7jxX7vvSZ1RA2Mqujnvay1eSF2",
         #remember_token: null,
         created_at: "2021-10-05 18:20:22",
         updated_at: "2021-10-05 18:20:22",
       },
       App\Models\User {#4289
         id: 2,
         name: "Sally",
         email: "sally@email.com",
         email_verified_at: null,
         #password: "$2y$10$LNQiHPxYRgVMq70ehswdjuMcXS2R435SknLIpLxONbrcNtZdIw9zS",
         #remember_token: null,
         created_at: "2021-10-05 18:21:54",
         updated_at: "2021-10-05 18:21:54",
       },
     ],
   }
>>>
```
---

### Make a Post Model and Migration
Borrados archivo de `/Models/Post.php` 
Crear nueva migracion de post para crear la tabla
```console
php artisan make:migration create_posts_table
```
Agregamos los campos necesarios a la migracion
```php
class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('excerpt');
            $table->text('body');
            $table->timestamps();
            $table->timestamp('published_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}

```
Se ejecuta la migracion desde webserver
```console
php artisan migrate
```
Crear modelo `Post`
```console
php artisan make:model Post
```
Usar Modelo Post y crear nuevo post desde tinker
```console
php artisan tinker
Psy Shell v0.10.8 (PHP 7.4.21 — cli) by Justin Hileman
>>> $post = new Post();
[!] Aliasing 'Post' to 'App\Models\Post' for this Tinker session.
=> App\Models\Post {#3499}
>>> $post->title = 'My First Post'
=> "My First Post"
>>> $post->excerpt = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
>>> $post->body = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus hendrerit sapien sed commodo. Curabitur vulputate
ex consectetur, tempor eros eu, laoreet libero. Vivamus vitae dapibus arcu. Duis vitae faucibus sapien, non auctor risus. Etiam finibus nunc nec dolor tincidunt, sed ullamcorper metus efficitur. Morbi vestibulum eleifend massa et convallis. Duis consectetur euismod massa a aliquet. Sed aliquet tempus mauris. Nulla facilisi. Pellentesque accumsan sagittis erat nec dapibus. Mauris erat metus, aliquam vel mauris quis, pulvinar accumsan lectus. Integer vel ante id neque vehicula rhoncus. Nullam vulputate vestibulum dui, ut ornare est.';
=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus hendrerit sapien sed commodo. Curabitur vulputate ex consectetur, tempor eros eu, laoreet libero. Vivamus vitae dapibus arcu. Duis vitae faucibus sapien, non auctor risus. Etiam finibus nunc nec dolor tincidunt, sed ullamcorper metus efficitur. Morbi vestibulum eleifend massa et convallis. Duis consectetur euismod massa a aliquet. Sed aliquet tempus mauris. Nulla facilisi. Pellentesque accumsan sagittis erat nec dapibus. Mauris erat metus, aliquam vel mauris quis, pulvinar accumsan lectus. Integer vel ante id neque vehicula rhoncus. Nullam vulputate vestibulum dui, ut ornare est."
>>> $post->save();
=> true
>>> Post::count();
=> 1
>>> Post:all();
PHP Fatal error:  Call to undefined function all() in Psy Shell code on line 1
>>> Post::all();
=> Illuminate\Database\Eloquent\Collection {#3817
     all: [
       App\Models\Post {#4185
         id: 1,
         title: "My First Post",
         excerpt: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
         body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus hendrerit sapien sed commodo. Curabitur vulputate ex consectetur, tempor eros eu, laoreet libero. Vivamus vitae dapibus arcu. Duis vitae faucibus sapien, non auctor risus. Etiam finibus nunc nec dolor tincidunt, sed ullamcorper metus efficitur. Morbi vestibulum eleifend massa et convallis. Duis consectetur euismod massa a aliquet. Sed aliquet tempus mauris. Nulla facilisi. Pellentesque accumsan sagittis erat nec dapibus. Mauris erat metus, aliquam vel mauris quis, pulvinar accumsan lectus. Integer vel ante id neque vehicula rhoncus. Nullam vulputate vestibulum dui, ut ornare est.",
         created_at: "2021-10-05 18:39:30",
         updated_at: "2021-10-05 18:39:30",
         published_at: null,
       },
     ],
   }
>>>
```
Cambiar ruta de `posts/{$post}` con el `id` en vez de el `slug`
```php
Route::get('/posts/{post}', function ($id) {
    return view('post',[
        'post' => Post::findOrFail($id)
    ]);
});
```
Cambiar link de la vista posts para que envie el `id` en vez del `slug`
```html
<a href="/posts/{{$post->id}}">
    {{ $post->title }}
</a>
```
---

### Eloquent Updates and HTML Escaping
Actualizar un Post con Eloquent desde tinker
```console
php artisan tinker
Psy Shell v0.10.8 (PHP 7.4.21 — cli) by Justin Hileman
>>> $post = Post::first();
[!] Aliasing 'Post' to 'App\Models\Post' for this Tinker session.
=> App\Models\Post {#4222
     id: 1,
     title: "My First Post",
     excerpt: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
     body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus hendrerit sapien sed commodo. Curabitur vulputate ex consectetur, tempor eros eu, laoreet libero. Vivamus vitae dapibus arcu. Duis vitae faucibus sapien, non auctor risus. Etiam finibus nunc nec dolor tincidunt, sed ullamcorper metus efficitur. Morbi vestibulum eleifend massa et convallis. Duis consectetur euismod massa a aliquet. Sed aliquet tempus mauris. Nulla facilisi. Pellentesque accumsan sagittis erat nec dapibus. Mauris erat metus, aliquam vel mauris quis, pulvinar accumsan lectus. Integer vel ante id neque vehicula rhoncus. Nullam vulputate vestibulum dui, ut ornare est.",
     created_at: "2021-10-05 18:39:30",
     updated_at: "2021-10-05 18:39:30",
     published_at: null,
   }
>>> $post->body = "<p>".$post->body."</p>";
=> "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus hendrerit sapien sed commodo. Curabitur vulputate ex consectetur, tempor eros eu, laoreet libero. Vivamus vitae dapibus arcu. Duis vitae faucibus sapien, non auctor risus. Etiam finibus nunc nec dolor tincidunt, sed ullamcorper metus efficitur. Morbi vestibulum eleifend massa et convallis. Duis consectetur euismod massa a aliquet. Sed aliquet tempus mauris. Nulla facilisi. Pellentesque accumsan sagittis erat nec dapibus. Mauris erat metus, aliquam vel mauris quis, pulvinar accumsan lectus. Integer vel ante id neque vehicula rhoncus. Nullam vulputate vestibulum dui, ut ornare est.</p>"
>>> $post->save();
=> true
>>> $post = Post::find(2);
=> App\Models\Post {#3501
     id: 2,
     title: "Eloquent is amazing",
     excerpt: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
     body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus hendrerit sapien sed commodo. Curabitur vulputate ex consectetur, tempor eros eu, laoreet libero. Vivamus vitae dapibus arcu. Duis vitae faucibus sapien, non auctor risus. Etiam finibus nunc nec dolor tincidunt, sed ullamcorper metus efficitur. Morbi vestibulum eleifend massa et convallis. Duis consectetur euismod massa a aliquet. Sed aliquet tempus mauris. Nulla facilisi. Pellentesque accumsan sagittis erat nec dapibus. Mauris erat metus, aliquam vel mauris quis, pulvinar accumsan lectus. Integer vel ante id neque vehicula rhoncus. Nullam vulputate vestibulum dui, ut ornare est.",
     created_at: "2021-10-05 18:49:20",
     updated_at: "2021-10-05 18:49:20",
     published_at: null,
   }
>>> $post->body = "<p>".$post->body."</p>";
=> "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus hendrerit sapien sed commodo. Curabitur vulputate ex consectetur, tempor eros eu, laoreet libero. Vivamus vitae dapibus arcu. Duis vitae faucibus sapien, non auctor risus. Etiam finibus nunc nec dolor tincidunt, sed ullamcorper metus efficitur. Morbi vestibulum eleifend massa et convallis. Duis consectetur euismod massa a aliquet. Sed aliquet tempus mauris. Nulla facilisi. Pellentesque accumsan sagittis erat nec dapibus. Mauris erat metus, aliquam vel mauris quis, pulvinar accumsan lectus. Integer vel ante id neque vehicula rhoncus. Nullam vulputate vestibulum dui, ut ornare est.</p>"
>>> $post->save();
=> true
>>> $post = Post::first();
=> App\Models\Post {#3817
     id: 1,
     title: "My First Post",
     excerpt: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
     body: "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus hendrerit sapien sed commodo. Curabitur vulputate ex consectetur, tempor eros eu, laoreet libero. Vivamus vitae dapibus arcu. Duis vitae faucibus sapien, non auctor risus. Etiam finibus nunc nec dolor tincidunt, sed ullamcorper metus efficitur. Morbi vestibulum eleifend massa et convallis. Duis consectetur euismod massa a aliquet. Sed aliquet tempus mauris. Nulla facilisi. Pellentesque accumsan sagittis erat nec dapibus. Mauris erat metus, aliquam vel mauris quis, pulvinar accumsan lectus. Integer vel ante id neque vehicula rhoncus. Nullam vulputate vestibulum dui, ut ornare est.</p>",
     created_at: "2021-10-05 18:39:30",
     updated_at: "2021-10-05 19:15:01",
     published_at: null,
   }
>>> $post->title = "My <strong>First></strong> Post";
=> "My <strong>First</strong> Post"
>>> $post->save();
=> true

```
Cambiar  linea del titulo en la vista de post para que imprima HTML
```html
<h1>{!! $post->title !!}</h1>
```

---


### 3 Ways to Mitigate Mass Assignment
Agregar variable fillable al modelo Post para saber que campos son los que seran utilizados para cambiar data, y asi evitar que otros datos que no deberian cambiar sean cambiados
**Formas de proteger Mass Assignment**

**Primer Forma**
```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $fillable = ['title','excerpt','body'];
}
```
**Segunda Forma**
```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
}
```
**Tercer Forma es NUNCA hacer esto**
```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $guarded = [];
}
```

---
### Route Model Binding
Binding de Laravel que automaticamente obtiene en base al parametro de la URL de la ruta el objeto del Modelo, este es un ejemplo de como quedaría la ruta buscando en el modelo el id por default, importante que la variable tiene que coincidir con el nombre del modelo

```php
Route::get('/posts/{post}', function (Post $post) {
    return view('post',[
        'post' => $post
    ]);
});
```
Para que busque no por id sino por un campo en especifico del modelo/tabla, en este caso el slug
```php
Route::get('/posts/{post:slug}', function (Post $post) {
    return view('post',[
        'post' => $post
    ]);
});
```
---

### Your First Eloquent Relationship

Para crear una relacion con eloquent, vamos a agregar categorias a los posts

Creamos modelo y migracion

```console
php artisan make:model Category -m
```
Agregamos campos necesarios a la migracion `Category`
```php
public function up()
{
    Schema::create('categories', function (Blueprint $table) {
        $table->id();
        $table->string('name');
        $table->string('slug');
        $table->timestamps();
    });
}
```
Creamos primer relacion en la migracion de `Post`, ligando el `category_id`
```php
public function up()
{
    Schema::create('posts', function (Blueprint $table) {
        $table->id();
        $table->foreignId('category_id');
        $table->string('slug')->unique();
        $table->string('title');
        $table->text('excerpt');
        $table->text('body');
        $table->timestamps();
        $table->timestamp('published_at')->nullable();
    });
}
```

Corremos nuevamente refresh de las tablas
```console
php artisan migrate:fresh
```

Creamos nuevas categorias en la base de datos

```console
php artisan tinker
Psy Shell v0.10.8 (PHP 7.4.21 — cli) by Justin Hileman
>>> $c = new Category
[!] Aliasing 'Category' to 'App\Models\Category' for this Tinker session.
=> App\Models\Category {#3498}
>>> $c->name="Personal"
=> "Personal"
>>> $c->slug="personal";
=> "personal"
>>> $c->save();
=> true
>>> $c = new Category;
=> App\Models\Category {#4290}
>>> $c->name="Work"
=> "Work"
>>> $c->slug="work";
=> "work"
>>> $c->save();
=> true
>>> $c = new Category;
=> App\Models\Category {#3500}
>>> $c->name="Hobbies"
=> "Hobbies"
>>> $c->slug="hobbies";
=> "hobbies"
>>> $c->save();
=> true
>>> 
```

Crear primer `Post` con una categoria asignada
```console
>>> Post::create(['title'=>'My Family Post','excerpt'=>'Excerpt for my post','body'=>'Lorem ipsum dolar sit amet.','slug'=>'my-family-post','category_id'=>1])
[!] Aliasing 'Post' to 'App\Models\Post' for this Tinker session.
=> App\Models\Post {#4378
     title: "My Family Post",
     excerpt: "Excerpt for my post",
     body: "Lorem ipsum dolar sit amet.",
     slug: "my-family-post",
     category_id: 1,
     updated_at: "2021-10-05 21:47:14",
     created_at: "2021-10-05 21:47:14",
     id: 1,
   }
>>> 


```
Para crear la primer relacion a nivel de Eloquent, hay que agregar nuevo metodo donde indique a quien pertenece y ya tendremos acceso a la info de la categoria
```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $fillable = ['title','excerpt','body','slug','category_id'];

    public function category(){
        return $this->belongsTo(Category::class);
    }
}

```
Ver data de una relacion

```console
$post = Post::first();
=> App\Models\Post {#4185
     id: 1,
     category_id: 1,
     slug: "my-family-post",
     title: "My Family Post",
     excerpt: "Excerpt for my post",
     body: "Lorem ipsum dolar sit amet.",
     created_at: "2021-10-05 21:47:14",
     updated_at: "2021-10-05 21:47:14",
     published_at: null,
   }
>>> $post->category
=> App\Models\Category {#3817
     id: 1,
     name: "Personal",
     slug: "personal",
     created_at: "2021-10-05 21:30:15",
     updated_at: "2021-10-05 21:30:15",
   }
>>> $post
=> App\Models\Post {#4185
     id: 1,
     category_id: 1,
     slug: "my-family-post",
     title: "My Family Post",
     excerpt: "Excerpt for my post",
     body: "Lorem ipsum dolar sit amet.",
     created_at: "2021-10-05 21:47:14",
     updated_at: "2021-10-05 21:47:14",
     published_at: null,
     category: App\Models\Category {#3817
       id: 1,
       name: "Personal",
       slug: "personal",
       created_at: "2021-10-05 21:30:15",
       updated_at: "2021-10-05 21:30:15",
     },
   }
>>> 

```

En la vista `posts` agregamos el nombre de la categoria
```php
@extends('layout')

@section('content')
    @foreach ($posts as $post)
        <article>
            <h1>
                <a href="/posts/{{$post->slug}}">
                    {!! $post->title !!}
                </a>
            </h1>

            <p>
                <a href="#">{{$post->category->name}}</a>
            </p>

            <div>
                {{ $post->excerpt }}
            </div>
        </article>
    @endforeach
@endsection
```
---

### Show All Posts Associated With a Category
Crear nueva ruta
```php
Route::get('categories/{category:slug}',function(Category $category){
    return view('posts',[
        'posts' => $category->posts
    ]);
});

```
Crear relacion de Category posee muchos Posts, en el modelo de Category
```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    public function posts(){
        return $this->hasMany(Post::class);
    }
}

```

En la vista de `posts` cambiamos el url de la categoria y ahora podremos acceder a todos los posts de una categoria especifica
```php
<p>
    <a href="/categories/{{ $post->category->slug }}">{{$post->category->name}}</a>
</p>
```

---


### Clockwork, and the N+1 Problem
Evitar problema N+1, el problema en si es que cuando se obtiene un query con una relacion, este hace mas querys apartes para obtener la info de la relacion, entonces en una sola carga podremos tener muchas query y esto puede hacer la pagina lenta, para ver esto vamos a utilizar el paquete clockwork para poder ver o depurar la info de las query que se ejecutan en cada pagina de nuestro proyecto

Instalar paquete clockwork, ademas instalar extension para el navegador
```console
composer require itsgoingd/clockwork
```
Para optimizar el query de obtener todos los Post, veamos como queda la ruta ahora
```php
Route::get('/', function () {
    return view('posts',[
        'posts'=> Post::with('category')->get()
    ]);
});
```
---

### Database seeding saves time
Agregamos nuevo campo de user_id a la tabla/migracion de posts
```php
public function up()
{
    Schema::create('posts', function (Blueprint $table) {
        $table->id();
        $table->foreignId('user_id');
        $table->foreignId('category_id');
        $table->string('slug')->unique();
        $table->string('title');
        $table->text('excerpt');
        $table->text('body');
        $table->timestamps();
        $table->timestamp('published_at')->nullable();
    });
}
```

Ejecutamos en consola

```console
sudo php artisan migrate:fresh
```

Vamos a crear seeds de la base de datos para ahorrar tiempo despues de limpiar la base de datos con migrate:fresh


Agregarmos nuestro primer seed en el archivo `/database/seeders/DatabaseSeeder.php`
```php
<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = User::factory()->create();
        Category::create([
            'name' => 'Personal',
            'slug' => 'personal'
        ]);
        Category::create([
            'name' => 'Family',
            'slug' => 'family'
        ]);
        Category::create([
            'name' => 'Work',
            'slug' => 'work'
        ]);
    }
}

```
Ejecutamos este comando para hacer fresh a la base de datos y ademas ejecutar el seeder que insertara la informacion
```console
sudo php artisan migrate:fresh --seed
```

Se agrega la relacion de users-posts
**en modelo Post.php**
```php
public function user(){
    return $this->belongsTo(User::class);
}
```
**en modelo User.php**
```php
public function posts(){
    return $this->hasMany(Post::class);
}
```

Seeder final de la clase con user, categorias y posts

```php
public function run()
{
    User::truncate();
    Category::truncate();
    Post::truncate();

    $user = User::factory()->create();
    $personal = Category::create([
        'name' => 'Personal',
        'slug' => 'personal'
    ]);
    $family = Category::create([
        'name' => 'Family',
        'slug' => 'family'
    ]);
    $work = Category::create([
        'name' => 'Work',
        'slug' => 'work'
    ]);

    Post::create([
        'user_id' => $user->id,
        'category_id' => $family->id,
        'title' => 'My Family Post',
        'slug' => 'my-first-post',
        'excerpt' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>',
        'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus hendrerit sapien sed commodo. Curabitur vulputate ex consectetur, tempor eros eu, laoreet libero. Vivamus vitae dapibus arcu. Duis vitae faucibus sapien, non auctor risus. Etiam finibus nunc nec dolor tincidunt, sed ullamcorper metus efficitur.</p>'
    ]);

    Post::create([
        'user_id' => $user->id,
        'category_id' => $work->id,
        'title' => 'My Work Post',
        'slug' => 'my-work-post',
        'excerpt' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>',
        'body' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus hendrerit sapien sed commodo. Curabitur vulputate ex consectetur, tempor eros eu, laoreet libero. Vivamus vitae dapibus arcu. Duis vitae faucibus sapien, non auctor risus. Etiam finibus nunc nec dolor tincidunt, sed ullamcorper metus efficitur.</p>'
    ]);
}
```

---

### Turbo boost with factories
Crear factories para crear informacion de prueba/falsa para llenar de datos las tablas de la base de datos

Comando para crear Factory
```console
php artisan make:factory PostFactory
php artisan make:factory CategoryFactory
```

Agregamos los campos y el data que queremos a nuestros factorys
**PostFactory**
```php
<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'category_id' => Category::factory(),
            'title' => $this->faker->sentence,
            'slug' => $this->faker->slug,
            'excerpt' => $this->faker->sentence,
            'body' => $this->faker->sentence,
        ];
    }
}

```

**CategoryFactory**
```php
<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'slug' => $this->faker->slug
        ];
    }
}

```

Cambiamos DatabaseSeeder para utilizar los factory

```php
<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = User::factory()->create([
            'name' => 'John Doe'
        ]);
        Post::factory(5)->create([
            'user_id' => $user->id
        ]);

    }
}

```

Usamos para probar
```console
sudo php artisan migrate:fresh --seed
```
---

### View all posts by an Author
Vamos a crear nueva ruta donde se muestren todos los posts de un solo autor, usando la url con nombre de usuario

Agregamos campo username a migracion de users
```php
public function up()
{
    Schema::create('users', function (Blueprint $table) {
        $table->id();
        $table->string('username')->unique();
        $table->string('name');
        $table->string('email')->unique();
        $table->timestamp('email_verified_at')->nullable();
        $table->string('password');
        $table->rememberToken();
        $table->timestamps();
    });
}
```
Modificamos el UserFactory para que genere nombres de usuario al azar
```php
public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'username' => $this->faker->unique()->userName,
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ];
    }
```
Cambiamos modelo Post para que la referencia de user en vez de llamarse user se llame author

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    protected $fillable = ['title','excerpt','body','slug','category_id'];

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function author(){
        return $this->belongsTo(User::class,'user_id');
    }
}

```

Cambiamos el URL de las vistas
```html
<p>
    By <a href="/authors/{{$post->author->username}}">{{$post->author->name}}</a> in <a href="/categories/{{ $post->category->slug }}">{{$post->category->name}}</a>
</p>
```

Y agregamos la nueva ruta
```php
Route::get('authors/{author:username}',function(User $author){
    return view('posts',[
        'posts' => $author->posts
    ]);
});
```
---
### Eager Load Relationship on an Existing Model

Vamos a ver una forma mas sencilla de obtener el eager load para no cargar muchas queries con las relaciones de los modelos

Agregamos al modelo de Post la siguiente variable

```php
protected $with = ['category','author'];
```

De esta forma podemos quitar el 'with' que teniamos en la ruta home o '/'
```php
Route::get('/', function () {
    return view('posts',[
        'posts'=> Post::latest()->get()
    ]);
});
```
Ahora por default en cada query del Modelo Post va a incluir with con la categoria y el autor

Si en algun caso en especifico no se requiere categoria o autor, se usa metodo without, ejemplo

```php
Post::without('author')->first();
```


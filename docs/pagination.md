[< Go to index](../README.md)

## Paginacion

### Laughably Simple Pagination
Vamos a usar paginado que nos brinda Eloquent de forma sencilla


Modificamos metodo **index** de **PostController** agregando el paginate con la cantidad de resultados por pagina

```php
public function index(){
    return view('posts.index',[
        'posts'=> Post::latest()->filter(
            request(['search', 'category','author'])
        )->paginate(6)->withQueryString()
    ]);
}
```

Ahora debemos colocar los links en la vista **posts/index**, laravel se encarga de diseñar todo
```php
<x-layout>
    @include('posts._header')

    <main class="max-w-6xl mx-auto mt-6 lg:mt-20 space-y-6">
        @if ($posts->count()>0)
            <x-posts-grid :posts="$posts" />

            {{ $posts->links() }}
        @else
            <p class="text-center">No posts yet. Please check back later</p>
        @endif
    </main>

</x-layout>

```
Agregamos excepcion en la URL de page al cambiar la opcion del dropdown en **category-dropdown**
```html
<x-dropdown>
    <x-slot name="trigger">
        <button class="py-2 pl-3 pr-9 text-sm font-semibold lg:w-32 text-left flex lg:inline-flex">
            {{ isset($currentCategory) ? ucwords($currentCategory->name) : 'Categories' }}
            <x-icon name="down-arrow" class="absolute pointer-events-none" style="right:12px" />
        </button>
    </x-slot>

    <x-dropdown-item href="/?{{ http_build_query(request()->except('category','page')) }}" :active="request()->routeIs('home')">All</x-dropdown-item>
    @foreach ($categories as $category)
        <x-dropdown-item
            href="/?category={{ $category->slug }}&{{ http_build_query(request()->except('category','page')) }}"
            :active='request()->is("categories/{$category->slug}")'
        >{{ ucwords($category->name) }}</x-dropdown-item>
    @endforeach
</x-dropdown>

```

Opcionalmente podemos modificar el diseño del paginado publicando el diseño del paginado de laravel con este comando
```console
php artisan vendor:publish
```
Y seleccionar numero correspondiente a **Tag: laravel-pagination**


---
 
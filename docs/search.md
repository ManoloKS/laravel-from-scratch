[< Go to index](../README.md)

## Busqueda

### Search (The Messy Way)
Creamos un search de una forma rapida que funciona


Modificamos ruta de home para obtener search y filtrar

```php
Route::get('/', function () {
    $posts = Post::latest();
    if(request('search')){
        $posts->where('title', 'like', '%'.request('search').'%')
              ->orWhere('body', 'like', '%'.request('search').'%');
    }
    return view('posts',[
        'posts'=> $posts->get(),
        'categories'=> Category::all()
    ]);
})->name("home");
```

En la vista _post-header Modificamos el formulario de busqueda que se encuentra al final del archivo
```html
<div class="relative flex lg:inline-flex items-center bg-gray-100 rounded-xl px-3 py-2">
    <form method="GET" action="#">
        <input type="text" name="search" placeholder="Find something"
                class="bg-transparent placeholder-black font-semibold text-sm"
                value="{{ request('search') }}">
    </form>
</div>
```

---

### Search (The cleaner way)
Vamos a mejorar el metodo search, y ya que vamos a utilizar mas logica, lo mejor es empezar creando un controller para dejar de usar el archivo de routes para que este se vea mas limpio

Creamos nuestro controller
```console
php artisan make:controller PostController
```

Agregamos metodo scope para filter en modelo de `Post`
```php
public function scopeFilter($query, array $filters){
    $query->when($filters["search"] ?? false, fn($query, $search) =>
        $query->where('title', 'like', '%'.$search.'%')
                ->orWhere('body', 'like', '%'.$search.'%'));
}
```

Agregamos los metodos index y show al controller
```php
<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(){
        return view('posts',[
            'posts'=> Post::latest()->filter(request(['search']))->get(),
            'categories'=> Category::all()
        ]);
    }

    public function show(Post $post) {
        return view('post',[
            'post' => $post
        ]);
    }
}

```

Y actualizamos el archivo `routes` utilizando el PostController y para que se vea mas limpio

```php
Route::get('/', [PostController::class, 'index'])->name("home");

Route::get('/posts/{post:slug}',[PostController::class, 'show']);
```

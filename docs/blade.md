[< Go to index](../README.md)

## Blade

### Blade: The Absolute Basics
Basico de blade como imprimir texto mas facilmente con y como utilizar foreach y if
Archivo view de `posts` queda así
```html
<!DOCTYPE html>

<title>My Blog</title>
<link rel="stylesheet" href="/app.css">
<script src="/app.js"></script>
<body>
    @foreach ($posts as $post)
        <article>
            <h1>
                    <a href="/posts/{{$post->slug}}">
                        {{ $post->title }}
                    </a>
                </h1>
            <div>
                {{ $post->excerpt }}
            </div>
        </article>
    @endforeach
</body>
```

Y view `post`
```html
<!DOCTYPE html>

<title>My Blog</title>
<link rel="stylesheet" href="/app.css">
<script src="/app.js"></script>
<body>
    <article>
        <h1>{{ $post->title }}</h1>
        <div>
            {!! $post->body !!}
        </div>
    </article>
    <a href="/">Go Back</a>
</body>
```

### Blade Layouts Two Ways
Uso de Layouts para evitar tener que usar toda la estructura HTML en todas las vistas y tener mas organizado la estructura de HTML en un archivo y el contenido dinamico en otro

A mi me gusto la forma utilizando `@yield, @extends y @section` en vez de los `components y <x-layout>`
Ahora creamos una vista de `layout` que contiene

```html
<!DOCTYPE html>

<title>My Blog</title>
<link rel="stylesheet" href="/app.css">
<body>
    @yield('content')
</body>

```

Y las vistas actuales ahora quedaran así
**posts**
```html
@extends('layout')

@section('content')
    @foreach ($posts as $post)
        <article>
            <h1>
                    <a href="/posts/{{$post->slug}}">
                        {{ $post->title }}
                    </a>
                </h1>
            <div>
                {{ $post->excerpt }}
            </div>
        </article>
    @endforeach
@endsection

```
**post**

```html
@extends('layout')

@section('content')
    <article>
        <h1>{{ $post->title }}</h1>
        <div>
            {!! $post->body !!}
        </div>
    </article>
    <a href="/">Go Back</a>
@endsection

```
---
### A Few Tweaks and Consideration
Se crea nuevo metodo findOrFail y se modifica find en modelo de Post.php para dejar ruta de /posts/{post} mas limpia
```php
    public static function find($slug){
        return static::all()->firstWhere('slug',$slug);
    }

    public static function findOrFail($slug){
        $post = static::all()->firstWhere('slug',$slug);
        if(!$post){
            throw new ModelNotFoundException();
        }
        return $post;
    }
```

Se quita metodo where() con expresion regular de la ruta
```php
Route::get('/posts/{post}', function ($slug) {
    return view('post',[
        'post' => Post::findOrFail($slug)
    ]);
});
```
---
[< Go to index](../README.md)

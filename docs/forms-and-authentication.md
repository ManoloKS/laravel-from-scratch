[< Go to index](../README.md)

## Formularios y autenticación

### Build a Register User Page
Vamos a crear una pagina de registro para usuarios

Creamos controlador de registro

```console
php artisan make:controller RegisterController
```

Creamos directorio para register en los views con su respectivo archivo `/views/register/create.blade.php`

Este archivo (`create.blade.php`) le agregamos el diseño del formulario de registro

```html
<x-layout>

    <section class="px-6 py-8">
        <main class="max-w-lg mx-auto mt-10 bg-gray-100 p-6 rounded-xl border-gray-200">
            <h1 class="text-center font-bold text-xl">Register!</h1>
            <form method="POST" action="/register" class="mt-10">
                @csrf
                <div class="mb-6">
                    <label for="name" class="block mb-2 uppercase font-bold text-xs text-gray-700">Name</label>
                    <input class="border border-gray-400 p-2 w-full" type="text" name="name" id="name" required>
                </div>

                <div class="mb-6">
                    <label for="username" class="block mb-2 uppercase font-bold text-xs text-gray-700">Username</label>
                    <input class="border border-gray-400 p-2 w-full" type="text" name="username" id="username" required>
                </div>

                <div class="mb-6">
                    <label for="email" class="block mb-2 uppercase font-bold text-xs text-gray-700">Email</label>
                    <input class="border border-gray-400 p-2 w-full" type="text" name="email" id="email" required>
                </div>

                <div class="mb-6">
                    <label for="password" class="block mb-2 uppercase font-bold text-xs text-gray-700">Password</label>
                    <input class="border border-gray-400 p-2 w-full" type="password" name="password" id="password" required>
                </div>

                <div class="mb-6">
                    <button class="bg-blue-400 text-white rounded py-2 px-4 hover:bg-blue-500" type="submit">Submit</button>
                </div>
            </form>
        </main>
    </section>

</x-layout>

```


Agregamos 2 nuevas rutas al archivo de **routes.php** haciendo referencia al nuevo controller
```php
Route::get('register', [RegisterController::class, 'create']);
Route::post('register', [RegisterController::class, 'store']);
```

Y agregamos los metodos al controller, donde hacemos return a la vista **create** y hacemos store del form de registro, creando la validacion de campos en el metodo de **store**
```php
<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function create(){

        return view('register.create');
    }

    public function store(){
        $attributes = request()->validate([
            'name' => 'required|max:255',
            'username' => 'required|max:255|min:3',
            'email' => 'required|email|max:255',
            'password' => 'required|min:7|max:255'
        ]);

        User::create($attributes);

        return redirect('/');
    }
}

```
Con esto ya podemos crear usuarios desde el url de **/register**

---

### Automatic Password Hashing With Mutators
Vamos a actualizar el modelo **User.php** para que cada vez que se realiza un create sobre el modelo User, la contraseña automaticamente sea encriptada con el metodo bcrypt() de laravel, para esto agregamos el siguiente mutation/metodo a nuestro modelo **User** y así de sencillo el password no sera visible y estara protegido en la base de datos
```php
public function setPasswordAttribute($password){
    $this->attributes["password"] = bcrypt($password);
}
```

---

### Failed Validation and Old Input Data
Al momento de que la validación de laravel falla y retorna al formulario, debemos mostrar el error al usuario, ademas de cargar automaticamente los datos que ya el usuario habia ingresado para que no tenga que ingresar todo nuevamente

Agregamos validacion al metodo validate en nuestro metodo de store en **RegisterController** para que se valide un unico usuario y email

```php
public function store(){
    $attributes = request()->validate([
        'name' => 'required|max:255',
        'username' => 'required|min:3|max:255|unique:users,username',
        'email' => 'required|email|max:255|unique:users,email',
        'password' => 'required|min:7|max:255'
    ]);

    User::create($attributes);

    return redirect('/');
}
```

Agregamos metodos de laravel **@error** y **old()** para mostrar los errores que tuvo el usuario en el form y cargar info vieja del form

Vista **create** queda así
```html
<x-layout>

    <section class="px-6 py-8">
        <main class="max-w-lg mx-auto mt-10 bg-gray-100 p-6 rounded-xl border-gray-200">
            <h1 class="text-center font-bold text-xl">Register!</h1>
            <form method="POST" action="/register" class="mt-10">
                @csrf
                <div class="mb-6">
                    <label for="name" class="block mb-2 uppercase font-bold text-xs text-gray-700">Name</label>
                    <input class="border border-gray-400 p-2 w-full" type="text" name="name" id="name" value="{{ old('name') }}" required>
                </div>
                @error('name')
                    <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                @enderror

                <div class="mb-6">
                    <label for="username" class="block mb-2 uppercase font-bold text-xs text-gray-700">Username</label>
                    <input class="border border-gray-400 p-2 w-full" type="text" name="username" id="username" value="{{ old('username') }}" required>
                </div>
                @error('username')
                    <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                @enderror

                <div class="mb-6">
                    <label for="email" class="block mb-2 uppercase font-bold text-xs text-gray-700">Email</label>
                    <input class="border border-gray-400 p-2 w-full" type="text" name="email" id="email" value="{{ old('email') }}" required>
                </div>
                @error('email')
                    <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                @enderror

                <div class="mb-6">
                    <label for="password" class="block mb-2 uppercase font-bold text-xs text-gray-700">Password</label>
                    <input class="border border-gray-400 p-2 w-full" type="password" name="password" id="password" required>
                </div>
                @error('password')
                    <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                @enderror

                <div class="mb-6">
                    <button class="bg-blue-400 text-white rounded py-2 px-4 hover:bg-blue-500" type="submit">Submit</button>
                </div>
            </form>
        </main>
    </section>

</x-layout>

```

---


### Show a success Flash Message
Vamos a mostrar un mensaje Flash, despues de registrar un usuario

En **RegisterController** agregamos el mensaje junto con el return redirect en el metodo store()
```php
public function store(){
    $attributes = request()->validate([
        'name' => 'required|max:255',
        'username' => 'required|min:3|max:255|unique:users,username',
        'email' => 'required|email|max:255|unique:users,email',
        'password' => 'required|min:7|max:255'
    ]);

    User::create($attributes);

    return redirect('/')->with('success','Your account has been created.');
}
```
Creamos nuevo component blade **/views/componenets/flash.blade.php** y agreamos el siguiente código

```html
@if (session()->has('success'))
    <div x-data="{ show: true }" x-init="setTimeout(()=> show = false, 4000)" x-show="show" class="fixed bottom-3 bg-blue-500 text-white py-2 px-4 rounded-xl right-3 text-sm">
        <p>{{ session('success') }}</p>
    </div>
@endif

```

Ahora desde el archivo **/components/layout.blade.php** llamamos el flash component

```html
<x-flash />
```

Con esto ya estamos mostrando un mensaje cada vez que un usuario es registrado

--- 

### Login and Logout
Ya tenemos registro listo, vamos a redireccionar el registro iniciando sesión al registrarse y ademas trabajaremos en el logout

Creamos controlador de SessionsController
```console
php artisan make:controller SessionsController
```

En el metodo store de **RegisterController**, agregamos metodo para hacer login
```php
public function store(){
    $attributes = request()->validate([
        'name' => 'required|max:255',
        'username' => 'required|min:3|max:255|unique:users,username',
        'email' => 'required|email|max:255|unique:users,email',
        'password' => 'required|min:7|max:255'
    ]);

    $user = User::create($attributes);

    Auth::login($user);

    return redirect('/')->with('success','Your account has been created.');
}
```

En el **layout** vamos a agregar botones de registro, login en caso de que no hay usuario logueado y dar la bienvenida en caso de que el usuario este logueado
```html
<!doctype html>

<title>Laravel From Scratch Blog</title>
<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/alpinejs/2.3.0/alpine.js" integrity="sha512-nIwdJlD5/vHj23CbO2iHCXtsqzdTTx3e3uAmpTm4x2Y8xCIFyWu4cSIV8GaGe2UNVq86/1h9EgUZy7tn243qdA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<body style="font-family: Open Sans, sans-serif">
    <section class="px-6 py-8">
        <nav class="md:flex md:justify-between md:items-center">
            <div>
                <a href="/">
                    <img src="/images/logo.svg" alt="Laracasts Logo" width="165" height="16">
                </a>
            </div>

            <div class="mt-8 md:mt-0 flex items-center">
                @auth
                    <span class="text-xs font-bold uppercase">Welcome, {{ auth()->user()->name }}!</span>

                    <form action="/logout" method="POST" class="text-xs font-semibold text-blue-500 ml-6">
                        @csrf
                        <button type="submit">Log Out</button>
                    </form>
                @else
                    <a href="/register" class="text-xs font-bold uppercase">Register</a>
                    <a href="/login" class="text-xs ml-6 font-bold uppercase">Login</a>
                @endauth


                <a href="#" class="bg-blue-500 ml-3 rounded-full text-xs font-semibold text-white uppercase py-3 px-5">
                    Subscribe for Updates
                </a>
            </div>
        </nav>

        {{$slot}}

        <footer class="bg-gray-100 border border-black border-opacity-5 rounded-xl text-center py-16 px-10 mt-16">
            <img src="/images/lary-newsletter-icon.svg" alt="" class="mx-auto -mb-6" style="width: 145px;">
            <h5 class="text-3xl">Stay in touch with the latest posts</h5>
            <p class="text-sm mt-3">Promise to keep the inbox clean. No bugs.</p>

            <div class="mt-10">
                <div class="relative inline-block mx-auto lg:bg-gray-200 rounded-full">

                    <form method="POST" action="#" class="lg:flex text-sm">
                        <div class="lg:py-3 lg:px-5 flex items-center">
                            <label for="email" class="hidden lg:inline-block">
                                <img src="/images/mailbox-icon.svg" alt="mailbox letter">
                            </label>

                            <input id="email" type="text" placeholder="Your email address"
                                   class="lg:bg-transparent py-2 lg:py-0 pl-4 focus-within:outline-none">
                        </div>

                        <button type="submit"
                                class="transition-colors duration-300 bg-blue-500 hover:bg-blue-600 mt-4 lg:mt-0 lg:ml-3 rounded-full text-xs font-semibold text-white uppercase py-3 px-8"
                        >
                            Subscribe
                        </button>
                    </form>
                </div>
            </div>
        </footer>
    </section>
    <x-flash />
</body>

```

Modificamos archivo **app/Providers/RouteServiceProvider.php** para cambiar la ruta por default del home
```php
public const HOME = '/';
```

Ahora agregamos la ruta logout y ademas agregamos middlewares a las rutas de registro para que solo accedan usuarios no logueados a dichas rutas
**routes.php**
```php
Route::get('register', [RegisterController::class, 'create'])->middleware('guest');
Route::post('register', [RegisterController::class, 'store'])->middleware('guest');

Route::post('logout',[SessionsController::class, 'destroy'])->middleware('auth');
```

Se agrega código necesario para **SessionsController**
```php
<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class SessionsController extends Controller
{
    public function destroy(){
        Auth::logout();
        return redirect('/')->with('success','Goodbye!');
    }
}

```

---

### Build the Login Page

Vamos a crear nuevo archivo de formulario para el login **/views/sessions/create.blade.php**
```html
<x-layout>

    <section class="px-6 py-8">
        <main class="max-w-lg mx-auto mt-10 bg-gray-100 p-6 rounded-xl border-gray-200">
            <h1 class="text-center font-bold text-xl">Log in!</h1>

            <form method="POST" action="/login" class="mt-10">
                @csrf

                <div class="mb-6">
                    <label for="email" class="block mb-2 uppercase font-bold text-xs text-gray-700">Email</label>
                    <input class="border border-gray-400 p-2 w-full" type="text" name="email" id="email" value="{{ old('email') }}" required>
                </div>
                @error('email')
                    <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                @enderror

                <div class="mb-6">
                    <label for="password" class="block mb-2 uppercase font-bold text-xs text-gray-700">Password</label>
                    <input class="border border-gray-400 p-2 w-full" type="password" name="password" id="password" required>
                </div>
                @error('password')
                    <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                @enderror

                <div class="mb-6">
                    <button class="bg-blue-400 text-white rounded py-2 px-4 hover:bg-blue-500" type="submit">Log In</button>
                </div>
            </form>
        </main>
    </section>

</x-layout>

```

Agregamos rutas de login, tanto la ruta get como la post en **routes.php**
```php
Route::get('login',[SessionsController::class, 'create'])->middleware('guest');
Route::post('login',[SessionsController::class, 'store'])->middleware('guest');
```

Ahora en **SessionsController** agregamos metodo store en el cual vamos a hacer toda la validacion y hacer el login
```php
<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class SessionsController extends Controller
{
    public function create(){
        return view('sessions.create');
    }

    public function store(){
        $attributes = request()->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if(Auth::attempt($attributes)){
            session()->regenerate();
            return redirect('/')->with('success','Welcome Back!');
        }

        throw ValidationException::withMessages([
            'email'=> 'Your provided credentials could not be verified.'
        ]);

    }

    public function destroy(){
        Auth::logout();
        return redirect('/')->with('success','Goodbye!');
    }
}

```

---




### Laravel Breeze Quick Peek
Organizamos un poco mejor el orden de **SessionsController** en el metodo **store** teniendo primero las validaciones y luego el log in como tal

```php
public function store(){
    $attributes = request()->validate([
        'email' => 'required|email',
        'password' => 'required'
    ]);

    if(!Auth::attempt($attributes)){
        throw ValidationException::withMessages([
            'email'=> 'Your provided credentials could not be verified.'
        ]);
    }

    session()->regenerate();

    return redirect('/')->with('success','Welcome Back!');

}
```

Vamos a utilizar un starter kit de Laravel

Vamos a utilizar laravel Breeze, pero para esto se hara en un proyecto aparte
```console
laravel new demo
```

Instalar breeze
```console
composer require laravel/breeze --dev 
```

```console
php artisan breeze:install
```

```console
npm install && npm run dev
```

Ahora se debe configurar nueva base de datos en el archivo **.env**
```console
DB_CONECTION=sqlite
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=/database/database.sqlite
DB_USERNAME=root
DB_PASSWORD=
```

Corremos la migracion de la base de datos
```console
php artisan migrate
```

**Con esto ya tenemos un proyecto nuevo listo con register, login, forgot password y logout integrado gratuitamente con laravel y queda libre a gusto del desarrollador para personalizarlo**

---

[< Go to index](../README.md)

## Admin Section
---

### Limit Access to Only Admins
Vamos a crear una sección para crear posts pero que solamente los administradores puedan acceder a ella

Creamos nuevo middleware
```console
php artisan make:middleware MustBeAdministrator
```

Agregamos validacion de usuario admin a **app/Http/MIddleware/MustBeAdministrator.php**
```php
<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MustBeAdministrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth()->user()->username != 'emanuel5'){
            abort(Response::HTTP_FORBIDDEN);
        }
        return $next($request);
    }
}

```

Creamos nueva vista para form de create en **resources/views/posts/create.blade.php**
```html
<x-layout>
    <section class="px-6 py-8">
        Hola
    </section>
</x-layout>

```

EN el archivo **app/Http/Kernel.php** buscamos la variable **$routeMiddleware** y agregamos nuestra referencia admin con el middleware creado
```php
 protected $routeMiddleware = [
    'auth' => \App\Http\Middleware\Authenticate::class,
    'admin' => MustBeAdministrator::class,
    'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
    'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
    'can' => \Illuminate\Auth\Middleware\Authorize::class,
    'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
    'password.confirm' => \Illuminate\Auth\Middleware\RequirePassword::class,
    'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
    'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
    'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
];
```

Creamos nueva ruta indicando que solo los admin pueden acceder por medio del middleware
```php
Route::get('admin/posts/create', [PostController::class, 'create'])->middleware('admin');
```

Creamos nuevo metodo create en **PostController**
```php
public function create(){
        
        return view('posts.create');
    }
}
```

---


### Create the Publish Post Form
Vamos a crear el formulario para crear nuevos posts

Agregamos diseño del formulario a **views/posts/create**
```html
<x-layout>
    <section class="px-6 py-8">
        <x-panel class="max-w-sm mx-auto">
            <form method="POST" action="/admin/posts">
                @csrf
                <div class="mb-6">
                    <label for="title" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                        Title
                    </label>
                    <input type="text" name="title" id="title" value="{{ old('title') }}" required class="border border-gray-400 p-2 w-full">
                    @error('title')
                        <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-6">
                    <label for="slug" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                        Slug
                    </label>
                    <input type="text" name="slug" id="slug" value="{{ old('slug') }}" required class="border border-gray-400 p-2 w-full">
                    @error('slug')
                        <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-6">
                    <label for="excerpt" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                        Excerpt
                    </label>
                    <textarea name="excerpt" id="excerpt" required class="border border-gray-400 p-2 w-full">{{ old('excerpt') }}</textarea>
                    @error('excerpt')
                        <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-6">
                    <label for="excerpt" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                        Body
                    </label>
                    <textarea name="body" id="body" required class="border border-gray-400 p-2 w-full">{{ old('body') }}</textarea>
                    @error('body')
                        <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-6">
                    <label for="category_id" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                        Category
                    </label>
                    <select name="category_id" id="category_id">
                        @foreach (\App\Models\Category::all() as $category)
                            <option value="{{ $category->id }} {{ old('category_id') == $category->id ? 'selected': '' }}">{{ ucwords($category->name) }}</option>
                        @endforeach
                    </select>
                    @error('category_id')
                        <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
                    @enderror
                </div>
                <x-submit-button>Publish</x-submit-button>
            </form>
        </x-panel>
    </section>
</x-layout>

```

Agregamos la ruta de store para los posts a **routes**
```php
Route::post('admin/posts', [PostController::class, 'store'])->middleware('admin');
```

Agregamos metodo **store** a **PostController**
```php
public function store(){
    $attributes = request()->validate([
        'title' => 'required',
        'slug' => ['required', Rule::unique('posts','slug')],
        'excerpt' => 'required',
        'body' => 'required',
        'category_id' => ['required', Rule::exists('categories','id')],
    ]);

    $attributes['user_id'] = auth()->id();


    Post::create($attributes);

    return redirect('/');
}
```
---

### Validate and Store Post Thumbnails
Ahora vamos a agregar opcion de agregar los thumbnails dentro de cada post al crear el post

En el archivo **config/filesystem** cambiamos el default FILESYSTEM_DRIVER por public
```php
'default' => env('FILESYSTEM_DRIVER', 'public'),
```

Cremos un enlace a la carpeta storage con public por medio de la consola
```console
php artisan storage:link
```

A la migración de **create_posts** le agregamos campo para thumbnails
```php
public function up()
{
    Schema::create('posts', function (Blueprint $table) {
        $table->id();
        $table->foreignId('user_id')->constrained()->cascadeOnDelete();
        $table->foreignId('category_id');
        $table->string('slug')->unique();
        $table->string('title');
        $table->string('thumbnail')->nullable();
        $table->text('excerpt');
        $table->text('body');
        $table->timestamps();
        $table->timestamp('published_at')->nullable();
    });
}
```

Modificamos formulario de **posts/create** agregando field de thumbnail
```html
<x-layout>
    <section class="py-8 max-w-md mx-auto">
        <h1 class="text-lg font-bold mb-4">Publish New Post</h1>
        <x-panel>
            <form method="POST" action="/admin/posts" enctype="multipart/form-data">
                @csrf
                <div class="mb-6">
                    <label for="title" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                        Title
                    </label>
                    <input type="text" name="title" id="title" value="{{ old('title') }}" required class="border border-gray-400 p-2 w-full">
                    @error('title')
                        <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-6">
                    <label for="slug" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                        Slug
                    </label>
                    <input type="text" name="slug" id="slug" value="{{ old('slug') }}" required class="border border-gray-400 p-2 w-full">
                    @error('slug')
                        <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-6">
                    <label for="thumbnail" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                        Thumbnail
                    </label>
                    <input type="file" name="thumbnail" id="thumbnail" value="{{ old('thumbnail') }}" required class="border border-gray-400 p-2 w-full">
                    @error('thumbnail')
                        <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-6">
                    <label for="excerpt" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                        Excerpt
                    </label>
                    <textarea name="excerpt" id="excerpt" required class="border border-gray-400 p-2 w-full">{{ old('excerpt') }}</textarea>
                    @error('excerpt')
                        <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-6">
                    <label for="excerpt" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                        Body
                    </label>
                    <textarea name="body" id="body" required class="border border-gray-400 p-2 w-full">{{ old('body') }}</textarea>
                    @error('body')
                        <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
                    @enderror
                </div>
                <div class="mb-6">
                    <label for="category_id" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                        Category
                    </label>
                    <select name="category_id" id="category_id">
                        @foreach (\App\Models\Category::all() as $category)
                            <option value="{{ $category->id }} {{ old('category_id') == $category->id ? 'selected': '' }}">{{ ucwords($category->name) }}</option>
                        @endforeach
                    </select>
                    @error('category_id')
                        <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
                    @enderror
                </div>
                <x-submit-button>Publish</x-submit-button>
            </form>
        </x-panel>
    </section>
</x-layout>

```

Ahora modificamos metodo store de **PostController** para guardar nuestro thumbnail
```php
 public function store(){
    $attributes = request()->validate([
        'title' => 'required',
        'thumbnail' => 'required|image',
        'slug' => ['required', Rule::unique('posts','slug')],
        'excerpt' => 'required',
        'body' => 'required',
        'category_id' => ['required', Rule::exists('categories','id')],
    ]);

    $attributes['user_id'] = auth()->id();
    $attributes['thumbnail'] = request()->file('thumbnail')->store('thumbnails');


    Post::create($attributes);

    return redirect('/');
}
```

Y mostramos la imagen en los diferentes archivos donde se llama la imagen del post
**posts/show**
```html
<img src="{{ asset('storage/'.$post->thumbnail) }}" alt="" class="rounded-xl">
```

**components/post-card** y **components/post-featured-card**
```html
<img src="{{ asset('storage/'.$post->thumbnail) }}" alt="Blog Post illustration" class="rounded-xl">
```

---


### Extract Form-Specific Blade Components

Ahora vamos a crear nuevos componentes para reorganizar y poder reutilizar los elementos de un form, y asi dejamos el form de create post mas sencillo a nivel de código

Creamos Carpeta **resources/views/components/form/**

Creamos nuevo archivo **components/form/label.blade.php**
```html
@props(['name'])
<label for="{{ $name }}" class="block mb-2 uppercase font-bold text-xs text-gray-700">
    {{ ucwords($name) }}
</label>

```

Creamos nuevo archivo **components/form/field.blade.php**
```html
<div class="mt-6">
    {{ $slot }}
</div>

```

Creamos nuevo archivo **components/form/input.blade.php**
```html
@props(['name', 'type' => 'text'])
<x-form.field>
    <x-form.label name="{{ $name }}" />
    <input type="{{ $type }}" name="{{ $name }}" id="{{ $name }}" value="{{ old($name) }}" required class="border border-gray-400 p-2 w-full">
    <x-form.error name="{{ $name }}" />
</x-form.field>

```

Creamos nuevo archivo **components/form/textarea.blade.php**
```html
@props(['name'])
<x-form.field>
    <x-form.label name="{{ $name }}" />
    <textarea name="{{ $name }}" id="{{ $name }}" required class="border border-gray-400 p-2 w-full">{{ old($name) }}</textarea>
    <x-form.error name="{{ $name }}" />
</x-form.field>

```

Creamos nuevo archivo **components/form/error.blade.php**
```html
@props(['name'])
@error($name)
    <p class="text-red-500 text-xs mt-2">{{ $message }}</p>
@enderror

```
Movemos archivo **components/submit-button** hacia **components/form** y lo renombramos por **components/form/button.blade.php**
```html
<x-form.field>
    <button class="bg-blue-500 text-white uppercase font-semibold text-xs py-2 px-10 rounded-2xl hover:bg-blue-600">
        {{ $slot }}
    </button>
</x-form.field>

```

Modificamos archivo **posts/create**
```html
<x-layout>
    <section class="py-8 max-w-md mx-auto">
        <h1 class="text-lg font-bold mb-4">Publish New Post</h1>
        <x-panel>
            <form method="POST" action="/admin/posts" enctype="multipart/form-data">
                @csrf
                <x-form.input name="title" />
                <x-form.input name="slug" />
                <x-form.input name="title" type="file" />
                <x-form.textarea name="excerpt" />
                <x-form.textarea name="body" />
                <x-form.field>
                    <x-form.label name="category" />
                    <select name="category_id" id="category_id">
                        @foreach (\App\Models\Category::all() as $category)
                            <option value="{{ $category->id }} {{ old('category_id') == $category->id ? 'selected': '' }}">{{ ucwords($category->name) }}</option>
                        @endforeach
                    </select>
                    <x-form.error name="category" />
                </x-form.field>

                <x-form.button>Publish</x-form.button>
            </form>
        </x-panel>
    </section>
</x-layout>

```

---

### Extend the Admin Layout
Vamos a mejorar la seccion administrativa

Primero modificamos **sessions/create** utilizando los componentes que ya hicimos anteriormente
```html
<x-layout>

    <section class="px-6 py-8">
        <main class="max-w-lg mx-auto mt-10">
            <x-panel>
            <h1 class="text-center font-bold text-xl">Log in!</h1>

            <form method="POST" action="/login" class="mt-10">
                @csrf

                <x-form.input name="email" type="email"/>
                <x-form.input name="password" type="password"/>
                <x-form.button>Log In</x-form.button>
            </form>
            </x-panel>
        </main>
    </section>

</x-layout>

```
Agregamos un poco mas de estilo al input en **components/form/input**
```html
@props(['name', 'type' => 'text'])
<x-form.field>
    <x-form.label name="{{ $name }}" />
    <input type="{{ $type }}" name="{{ $name }}" id="{{ $name }}" value="{{ old($name) }}" required class="border border-gray-200 p-2 w-full rounded">
    <x-form.error name="{{ $name }}" />
</x-form.field>

```

Mejoramos tambien el de **components/form/textarea**
```html
@props(['name'])
<x-form.field>
    <x-form.label name="{{ $name }}" />
    <textarea name="{{ $name }}" id="{{ $name }}" required class="border border-gray-200 p-2 w-full rounded">{{ old($name) }}</textarea>
    <x-form.error name="{{ $name }}" />
</x-form.field>
```

Actualizamos nuestro **layout** para agregar un dropdown donde esta nuestro nombre de usuario con diferentes enlaces de acceso 
```html
<!doctype html>

<title>Laravel From Scratch Blog</title>
<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/alpinejs/2.3.0/alpine.js" integrity="sha512-nIwdJlD5/vHj23CbO2iHCXtsqzdTTx3e3uAmpTm4x2Y8xCIFyWu4cSIV8GaGe2UNVq86/1h9EgUZy7tn243qdA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<body style="font-family: Open Sans, sans-serif">
    <section class="px-6 py-8">
        <nav class="md:flex md:justify-between md:items-center">
            <div>
                <a href="/">
                    <img src="/images/logo.svg" alt="Laracasts Logo" width="165" height="16">
                </a>
            </div>

            <div class="mt-8 md:mt-0 flex items-center">
                @auth
                    <x-dropdown>
                        <x-slot name="trigger">
                            <button class="text-xs font-bold uppercase">Welcome, {{ auth()->user()->name }}!</button>
                        </x-slot>
                        <x-dropdown-item href="/admin/dashboard">Dashboard</x-dropdown-item>
                        <x-dropdown-item href="/admin/posts/create" :active="request()->is('admin/posts/create')">New Post</x-dropdown-item>
                        <x-dropdown-item href="#" x-data="{}" @click.prevent="document.querySelector('#logout-form').submit()">Log Out</x-dropdown-item>
                        <form id="logout-form" action="/logout" method="POST" class="hidden">
                            @csrf
                        </form>
                    </x-dropdown>

                @else
                    <a href="/register" class="text-xs font-bold uppercase">Register</a>
                    <a href="/login" class="text-xs ml-6 font-bold uppercase">Login</a>
                @endauth


                <a href="#newsletter" class="bg-blue-500 ml-3 rounded-full text-xs font-semibold text-white uppercase py-3 px-5">
                    Subscribe for Updates
                </a>
            </div>
        </nav>

        {{$slot}}

        <footer id="newsletter" class="bg-gray-100 border border-black border-opacity-5 rounded-xl text-center py-16 px-10 mt-16">
            <img src="/images/lary-newsletter-icon.svg" alt="" class="mx-auto -mb-6" style="width: 145px;">
            <h5 class="text-3xl">Stay in touch with the latest posts</h5>
            <p class="text-sm mt-3">Promise to keep the inbox clean. No bugs.</p>

            <div class="mt-10">
                <div class="relative inline-block mx-auto lg:bg-gray-200 rounded-full">

                    <form method="POST" action="/newsletter" class="lg:flex text-sm">
                        @csrf
                        <div class="lg:py-3 lg:px-5 flex items-center">
                            <label for="email" class="hidden lg:inline-block">
                                <img src="/images/mailbox-icon.svg" alt="mailbox letter">
                            </label>

                            <div>
                                <input id="email" name="email" type="text" placeholder="Your email address"
                                   class="lg:bg-transparent py-2 lg:py-0 pl-4 focus-within:outline-none">
                                @error('email')
                                    <span class="text-xs text-red-500">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>

                        <button type="submit"
                                class="transition-colors duration-300 bg-blue-500 hover:bg-blue-600 mt-4 lg:mt-0 lg:ml-3 rounded-full text-xs font-semibold text-white uppercase py-3 px-8"
                        >
                            Subscribe
                        </button>
                    </form>
                </div>
            </div>
        </footer>
    </section>
    <x-flash />
</body>

```
Agregamos nuestro código base a **components/setting**, el cual tendra una barra lateral con links y nuestro formulario de crear posts
```html
@props(['heading'])
<section class="py-8 max-w-4xl mx-auto">
    <h1 class="text-lg font-bold mb-8 pb-2 border-b">{{ $heading }}</h1>
    <div class="flex">
        <aside class="w-48">
            <h4 class="font-semibold mb-4">Links</h4>

            <ul>
                <li>
                    <a href="/admin/dashboard" class="{{ request()->is('admin/dashboard') ? 'text-blue-500' : '' }}">Dashboard</a>
                </li>
                <li>
                    <a href="/admin/posts/create" class="{{ request()->is('admin/posts/create') ? 'text-blue-500' : '' }}">New Post</a>
                </li>
            </ul>
        </aside>
        <main class="flex-1">
            <x-panel>
                {{ $slot }}
            </x-panel>
        </main>
    </div>
</section>

```

Ahora actualizamos **posts/create** para adaptarlo con el component **setting**
```html
<x-layout>
    <x-setting heading="Publish New Post">
        <form method="POST" action="/admin/posts" enctype="multipart/form-data">
            @csrf
            <x-form.input name="title" />
            <x-form.input name="slug" />
            <x-form.input name="title" type="file" />
            <x-form.textarea name="excerpt" />
            <x-form.textarea name="body" />
            <x-form.field>
                <x-form.label name="category" />
                <select name="category_id" id="category_id">
                    @foreach (\App\Models\Category::all() as $category)
                        <option value="{{ $category->id }} {{ old('category_id') == $category->id ? 'selected': '' }}">{{ ucwords($category->name) }}</option>
                    @endforeach
                </select>
                <x-form.error name="category" />
            </x-form.field>

            <x-form.button>Publish</x-form.button>
        </form>
    </x-setting>
</x-layout>

```

---

### Create a Form to Edit and Delete Posts
En este capitulo vamos a crear una lista con todos los posts con la opcion de editar y eliminar

Creamos nuevo controlador para admin post
```console
php artisan make:controller AdminPostController
```

Eliminamos las siguientes rutas
```php
Route::get('admin/posts/create', [PostController::class, 'create'])->middleware('admin');
Route::post('admin/posts', [PostController::class, 'store'])->middleware('admin');
```
Debemos eliminar tambien los metodos **store** y **create** de **PostController** porque los vamos a migrar a **AdminPostController**


Creamos nueva carpeta en **/resources/views/admin/posts** y en dicha carpeta creamos nueva vista llamada **index**

Actualizamos algunos componentes
Actualizamos link de categoria en **resources/views/components/category-button.blade.php**
```html
@props(['category'])
<a href="/?category={{ $category->slug }}"
    class="px-3 py-1 border border-blue-300 rounded-full text-blue-300 text-xs uppercase font-semibold"
    style="font-size: 10px">{{ $category->name }}</a>

```

Agregamos default attribute a **resources/views/components/form/input.blade.php**
```html
@props(['name', 'type' => 'text'])
<x-form.field>
    <x-form.label name="{{ $name }}" />
    <input type="{{ $type }}" name="{{ $name }}" id="{{ $name }}" {{ $attributes(['value' => old($name)]) }} class="border border-gray-200 p-2 w-full rounded">
    <x-form.error name="{{ $name }}" />
</x-form.field>

```

Agregamos slot como opcional, sino mostrar old data en **resources/views/components/form/textarea.blade.php**
```html
@props(['name'])
<x-form.field>
    <x-form.label name="{{ $name }}" />
    <textarea name="{{ $name }}" id="{{ $name }}" required class="border border-gray-200 p-2 w-full rounded">{{ $slot ?? old($name) }}</textarea>
    <x-form.error name="{{ $name }}" />
</x-form.field>

```

Actualizamos boton de categoria en **resources/views/components/post-featured-card.blade.php**
```html
<div class="space-x-2">
    <x-category-button :category="$post->category" />
</div>
```

Se actualiza **resources/views/components/setting.blade.php**
```html
@props(['heading'])
<section class="py-8 max-w-4xl mx-auto">
    <h1 class="text-lg font-bold mb-8 pb-2 border-b">{{ $heading }}</h1>
    <div class="flex">
        <aside class="w-48 flex-shrink-0">
            <h4 class="font-semibold mb-4">Links</h4>

            <ul>
                <li>
                    <a href="/admin/posts" class="{{ request()->is('admin/posts') ? 'text-blue-500' : '' }}">All Posts</a>
                </li>
                <li>
                    <a href="/admin/posts/create" class="{{ request()->is('admin/posts/create') ? 'text-blue-500' : '' }}">New Post</a>
                </li>
            </ul>
        </aside>
        <main class="flex-1">
            <x-panel>
                {{ $slot }}
            </x-panel>
        </main>
    </div>
</section>

```

Agregamos todos los metodos resource a **AdminPostController**
```php
<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AdminPostController extends Controller
{
    public function index(){
        return view('admin.posts.index',[
            'posts' => Post::paginate(50)
        ]);
    }

    public function create(){

        return view('admin.posts.create');
    }

    public function store(){
        $attributes = request()->validate([
            'title' => 'required',
            'thumbnail' => 'required|image',
            'slug' => ['required', Rule::unique('posts','slug')],
            'excerpt' => 'required',
            'body' => 'required',
            'category_id' => ['required', Rule::exists('categories','id')],
        ]);

        $attributes['user_id'] = auth()->id();
        $attributes['thumbnail'] = request()->file('thumbnail')->store('thumbnails');


        Post::create($attributes);

        return redirect('/');
    }

    public function edit(Post $post){
        return view('admin.posts.edit',['post'=>$post]);
    }

    public function update(Post $post){
        $attributes = request()->validate([
            'title' => 'required',
            'thumbnail' => 'image',
            'slug' => ['required', Rule::unique('posts','slug')->ignore($post->id)],
            'excerpt' => 'required',
            'body' => 'required',
            'category_id' => ['required', Rule::exists('categories','id')],
        ]);
        if(isset($attributes["thumbnail"])){
            $attributes['thumbnail'] = request()->file('thumbnail')->store('thumbnails');
        }

        $post->update($attributes);

        return back()->with('success','Post Updated!');
    }

    public function destroy(Post $post){
        $post->delete();

        return back()->with('success','Post Deleted');
    }
}

```

Agregamos todas las nuevas rutas para ver, eliminar, crear, index, editar, actualizar en **routes**
```php
//admin
Route::post('admin/posts', [AdminPostController::class, 'store'])->middleware('admin');
Route::get('admin/posts/create', [AdminPostController::class, 'create'])->middleware('admin');
Route::get('admin/posts', [AdminPostController::class, 'index'])->middleware('admin');
Route::get('admin/posts/{post}/edit', [AdminPostController::class, 'edit'])->middleware('admin');
Route::patch('admin/posts/{post}', [AdminPostController::class, 'update'])->middleware('admin');
Route::delete('admin/posts/{post}', [AdminPostController::class, 'destroy'])->middleware('admin');
```

Se agrega la lista de posts en el dashboard **admin/posts/index**
```html
<x-layout>
    <x-setting heading="Manage Posts">
        <!-- This example requires Tailwind CSS v2.0+ -->
        <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table class="min-w-full divide-y divide-gray-200">
                    <tbody class="bg-white divide-y divide-gray-200">
                    @foreach ($posts as $post)
                        <tr>
                            <td class="px-6 py-4 whitespace-nowrap">
                                <div class="flex items-center">
                                    <div class="text-sm font-medium text-gray-900">
                                        <a href="/posts/{{ $post->slug }}">
                                            {{ $post->title }}
                                        </a>
                                    </div>
                                </div>
                            </td>
                            <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                <a href="/admin/posts/{{ $post->id }}/edit" class="text-blue-500 hover:text-blue-600">Edit</a>
                            </td>

                            <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                <form method="POST" action="/admin/posts/{{ $post->id }}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="text-xs text-gray-400">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        </div>

    </x-setting>
</x-layout>

```

Finalmente se agrega formulario de editar en **admin/posts/edit**
```html
<x-layout>
    <x-setting :heading="'Edit Post: '.$post->title">
        <form method="POST" action="/admin/posts/{{ $post->id }}" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <x-form.input name="title" :value="old('title', $post->title)" />
            <x-form.input name="slug" :value="old('title', $post->slug)" />
            <div class="flex mt-6">
                <div class="flex-1">
                    <x-form.input name="thumbnail" type="file" :value="old('thumbnail', $post->thumbnail)" />
                </div>
                <img src="{{ asset('storage/'.$post->thumbnail) }}" alt="" class="rounded-xl ml-6" width="100">
            </div>
            <x-form.textarea name="excerpt">{{ old('excerpt', $post->excerpt )}}</x-form.textarea>
            <x-form.textarea name="body">{{ old('body', $post->body )}}</x-form.textarea>
            <x-form.field>
                <x-form.label name="category" />
                <select name="category_id" id="category_id">
                    @foreach (\App\Models\Category::all() as $category)
                        <option value="{{ $category->id }}" {{ old('category_id', $post->category_id) == $category->id ? 'selected': '' }}>{{ ucwords($category->name) }}</option>
                    @endforeach
                </select>
                <x-form.error name="category" />
            </x-form.field>

            <x-form.button>Update</x-form.button>
        </form>
    </x-setting>
</x-layout>

```

---

### Group and Store Validation Logic
Ahora modificamos **AdminPostController** reutilizando el metodo validate separandolo en un nuevo metodo y dejando mas limpios los metodos store y update
```php
<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AdminPostController extends Controller
{
    public function index(){
        return view('admin.posts.index',[
            'posts' => Post::paginate(50)
        ]);
    }

    public function create(){

        return view('admin.posts.create');
    }

    public function store(){
        Post::create(array_merge($this->validatePost(),[
            'user_id' => request()->user()->id,
            'thumbnail' => request()->file('thumbnail')->store('thumbnails')
        ]));

        return redirect('/');
    }

    public function edit(Post $post){
        return view('admin.posts.edit',['post'=>$post]);
    }

    public function update(Post $post){
        $attributes = $this->validatePost($post);

        if(isset($attributes["thumbnail"])){
            $attributes['thumbnail'] = request()->file('thumbnail')->store('thumbnails');
        }

        $post->update($attributes);

        return back()->with('success','Post Updated!');
    }

    public function destroy(Post $post){
        $post->delete();

        return back()->with('success','Post Deleted');
    }

    protected function validatePost(Post $post = null){
        $post ??= new Post();

        return request()->validate([
            'title' => 'required',
            'thumbnail' => $post->exists ? ['image']:['required','image'],
            'slug' => ['required', Rule::unique('posts','slug')->ignore($post)],
            'excerpt' => 'required',
            'body' => 'required',
            'category_id' => ['required', Rule::exists('categories','id')],
        ]);
    }
}

```

---

### All About Authorization
En este capitulo vemos formas para dar autorizacion a los usuarios

En el archivo **app/Http/Providers/AppServiceProvider** agregamos en el metodo boot la clase de Gate para definir quien es admin
```php
public function boot()
{
    //
    Gate::define('admin',function(User $user){
        return $user->username == 'emanuel5';
    });
}
```

Un ejemplo para validar si el usuario logueado esta autorizado seria
```php
if(auth()->user()->can('admin')){
    //tiene permiso admin
}
```

En las vistas blade se puede utilizar @can
```html
@can('admin')

@endcan
```

Tambien podemos crear nuestra propia directiva blade en **AppServiceProvider**
```php
public function boot()
{
    //
    Gate::define('admin',function(User $user){
        return $user->username == 'emanuel5';
    });

    Blade::if('admin', function () {
        return request()->user()->can('admin');
    });
}
```

Eliminamos archivo **app/Http/Middleware/MustBeAdministrator.php** ya que no nos hace falta, y eliminados directiva de **admin** en el archivo **Kernel.php**


Usamos directiva **can** de blade para colocar las opciones de admin del archivo de **layout** solo para administradores 
```html
@can('admin')
    <x-dropdown-item href="/admin/posts" :active="request()->is('admin/posts')">Dashboard</x-dropdown-item>
    <x-dropdown-item href="/admin/posts/create" :active="request()->is('admin/posts/create')">New Post</x-dropdown-item>
@endcan
```

Ahora ademas podemos borrar todas las rutas de AdminPostController y colocar una ruta resource que larave automaticamente entiende para crear todas las rutas necesarias
```php
//admin
Route::middleware(['can:admin'])->group(function () {
    Route::resource('admin/posts', AdminPostController::class)->except('show');
});

```

---

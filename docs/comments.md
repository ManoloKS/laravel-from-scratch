[< Go to index](../README.md)

## Comments

---

### Write the Markup for a Post Comment
Vamos a empezar con sección de comentarios dentro de cada post

Creamos nuevo component llamado **views/components/post-comment.blade.php**
Dentro del mismo creamos el diseño HTML que tendran los comentarios
```html
<article class="flex bg-gray-100 p-6 rounded-xl border border-gray-200 space-x-4">
    <div class="flex-shrink-0">
        <img class="rounded-xl" src="https://i.pravatar.cc/100" width="60" height="60" alt="">
    </div>

    <div>
        <header class="mb-4">
            <h3 class="font-bold">John Doe</h3>
            <p class="text-xs">
                Posted
                <time>8 months ago</time>
            </p>
        </header>
        <p>
            Eligendi non ex mollitia quis in. Ea iure et rerum et. Dolorum deserunt quo fugiat consequatur esse. Nam qui adipisci sed consequuntur molestias ut quibusdam.

            Et nemo et reprehenderit. Qui natus minus hic vero. Impedit aut id numquam aperiam placeat possimus. Incidunt tempore et in rerum voluptas.
        </p>
    </div>
</article>

```

Ahora desde la vista **posts/show/** agregamos lo siguiente antes del dinal del tag `<article>`

```php
<section class="col-span-8 col-start-5 mt-10 space-y-6">
    <x-post-comment />
    <x-post-comment />
    <x-post-comment />
</section>
```
Con esto ya tenemos un diseño de los comentarios dentro de cada post

---

### Table Consistency and Foreign Key Constraints

Vamos a crear modelo de comentarios, controller y factory
```console
php artisan make:model Comment -mfc
```

Agregamos campos necesarios en la migracion creada de **create_comments_table**, en la cual agregamos llaves foreneas a los campos `post_id` y `user_id`
```php
public function up()
{
    Schema::create('comments', function (Blueprint $table) {
        $table->id();
        $table->foreignId('post_id')->constrained()->cascadeOnDelete();
        $table->foreignId('user_id')->constrained()->cascadeOnDelete();
        $table->text('body');
        $table->timestamps();
    });
}
```

Y corremos la migracion

```console
php artisan migrate
```

---

### Make the Comments Section Dynamic
Vamos a hacer los comentarios dinamicos obteniendo los comentarios desde la base de datos y cargandolos en cada post

Agregamos relaciones de tablas a los modelos de **Comment** y **Post**

**Comment**
```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    public function post(){
        return $this->belongsTo(Post::class);
    }

    public function author(){
        return $this->belongsTo(User::class, 'user_id');
    }
}

```

**Post**
```php
public function comments(){
    return $this->hasMany(Comment::class);
}

```


Cargamos los datos a generar en el **CommentFactory.php**
```php
<?php

namespace Database\Factories;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Comment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'post_id' => Post::factory(),
            'user_id' => User::factory(),
            'body' => $this->faker->paragraph()
        ];
    }
}

```

Ejecutamos comando para el factory y crear comentarios desde tinker
```console
php artisan tinker
```
```console
>>> Comment::factory(10)->create(["post_id" => 49]);
```

Ya tenemos comentarios en nuestra Base de datos, ahora vamos a modificar las vistas para cargar los datos

En **posts/show** agregamos component de **components/post-comment** dentro de un forech
```html
<section class="col-span-8 col-start-5 mt-10 space-y-6">
    @foreach ($post->comments as $comment)
        <x-post-comment :comment="$comment" />
    @endforeach
</section>
```

**components/post-comment**
```html
@props(['comment'])
<article class="flex bg-gray-100 p-6 rounded-xl border border-gray-200 space-x-4">
    <div class="flex-shrink-0">
        <img class="rounded-xl" src="https://i.pravatar.cc/60?u={{ $comment->id }}" width="60" height="60" alt="">
    </div>

    <div>
        <header class="mb-4">
            <h3 class="font-bold">{{ $comment->author->username }}</h3>
            <p class="text-xs">
                Posted
                <time>{{ $comment->created_at->diffForHumans() }}</time>
            </p>
        </header>
        <p>
           {{ $comment->body }}
        </p>
    </div>
</article>

```

---


### Design the Comment Form
Crear diseño formulario para crear comentarios a los posts

Creamos component **views/components/panel.blade.php**
```html
<div {{ $attributes(['class' => "border border-gray-200 p-6 rounded-xl"]) }}>
    {{ $slot }}
</div>


```

Agregamos formulario a la vista **posts/show**
```html
<x-layout>
    <section class="px-6 py-8">
        <main class="max-w-6xl mx-auto mt-10 lg:mt-20 space-y-6">
            <article class="max-w-4xl mx-auto lg:grid lg:grid-cols-12 gap-x-10">
                <div class="col-span-4 lg:text-center lg:pt-14 mb-10">
                    <img src="/images/illustration-1.png" alt="" class="rounded-xl">

                    <p class="mt-4 block text-gray-400 text-xs">
                        Published <time>{{ $post->created_at->diffForHumans() }}</time>
                    </p>

                    <div class="flex items-center lg:justify-center text-sm mt-4">
                        <img src="/images/lary-avatar.svg" alt="Lary avatar">
                        <div class="ml-3 text-left">
                            <h5 class="font-bold">
                                <a href="/?author={{ $post->author->username }}">{{ $post->author->name }}</a>
                            </h5>
                        </div>
                    </div>
                </div>

                <div class="col-span-8">
                    <div class="hidden lg:flex justify-between mb-6">
                        <a href="/"
                            class="transition-colors duration-300 relative inline-flex items-center text-lg hover:text-blue-500">
                            <svg width="22" height="22" viewBox="0 0 22 22" class="mr-2">
                                <g fill="none" fill-rule="evenodd">
                                    <path stroke="#000" stroke-opacity=".012" stroke-width=".5" d="M21 1v20.16H.84V1z">
                                    </path>
                                    <path class="fill-current"
                                        d="M13.854 7.224l-3.847 3.856 3.847 3.856-1.184 1.184-5.04-5.04 5.04-5.04z">
                                    </path>
                                </g>
                            </svg>

                            Back to Posts
                        </a>

                        <div class="space-x-2">
                            <x-category-button :category="$post->category" />
                        </div>
                    </div>

                    <h1 class="font-bold text-3xl lg:text-4xl mb-10">
                        {{ $post->title }}
                    </h1>

                    <div class="space-y-4 lg:text-lg leading-loose">
                        {!! $post->body !!}
                    </div>
                </div>
                <section class="col-span-8 col-start-5 mt-10 space-y-6">
                    <x-panel>
                        <form method="POST" action="#">
                            @csrf
                            <header class="flex items-center">
                                <img src="https://i.pravatar.cc/60?u={{ auth()->id() }}" alt="" width="40" height="40" class="rounded-full">
                                <h2 class="ml-4">Want to participate?</h2>
                            </header>
                            <div class="mt-6">
                                <textarea name="body" class="w-full text-sm focus:outline-none focus:ring" placeholder="Quick, thing of something to say!" rows="5"></textarea>
                            </div>
                            <div class="flex justify-end mt-6 border-t border-gray-200 pt-6">
                                <button class="bg-blue-500 text-white uppercase font-semibold text-xs py-2 px-10 rounded-2xl hover:bg-blue-600">POST</button>
                            </div>
                        </form>
                    </x-panel>
                    @foreach ($post->comments as $comment)
                        <x-post-comment :comment="$comment" />
                    @endforeach
                </section>
            </article>
        </main>
    </section>
</x-layout>

```

Utilizamos component **panel** para reutilizarlo en el component **post-comment** y **post-comment** quedaria asi
```html
@props(['comment'])
<x-panel class="bg-gray-500">
    <article class="flex space-x-4">
        <div class="flex-shrink-0">
            <img class="rounded-xl" src="https://i.pravatar.cc/60?u={{ $comment->id }}" width="60" height="60" alt="">
        </div>

        <div>
            <header class="mb-4">
                <h3 class="font-bold">{{ $comment->author->username }}</h3>
                <p class="text-xs">
                    Posted
                    <time>{{ $comment->created_at->diffForHumans() }}</time>
                </p>
            </header>
            <p>
               {{ $comment->body }}
            </p>
        </div>
    </article>
</x-panel>


```

---

### Activate the Comment Form
Creamos controlador para los comment form
```console
php artisan make:controller PostCommentsController
```

Agregamos nueva ruta para el store de comentarios
```php
Route::post('posts/{post:slug}/comments', [PostCommentsController::class,'store'])->middleware('auth');
```

En **x-panel** de vista **/posts/show** agregamos validacion al form para que solo usuarios registrados puedan verlo
```html
@auth
    <x-panel>
        <form method="POST" action="/posts/{{ $post->slug }}/comments">
            @csrf
            <header class="flex items-center">
                <img src="https://i.pravatar.cc/60?u={{ auth()->id() }}" alt="" width="40" height="40" class="rounded-full">
                <h2 class="ml-4">Want to participate?</h2>
            </header>
            <div class="mt-6">
                <textarea name="body" class="w-full text-sm focus:outline-none focus:ring" placeholder="Quick, thing of something to say!" rows="5"></textarea>
            </div>
            <div class="flex justify-end mt-6 border-t border-gray-200 pt-6">
                <button class="bg-blue-500 text-white uppercase font-semibold text-xs py-2 px-10 rounded-2xl hover:bg-blue-600">POST</button>
            </div>
        </form>
    </x-panel>
    @else
        <p class="font-semibold">
            <a href="/register" class="hover:underline">Register</a> or <a href="/login" class="hover:underline">Login</a> to leave a comment.
        </p>
    @endauth
```

Agregamos metodo store a **PostCommentsController** para validar y guardar el comentario
```php
<?php

namespace App\Http\Controllers;

use App\Models\Post;

class PostCommentsController extends Controller
{
    public function store(Post $post){

        request()->validate([
            'body' => 'required'
        ]);

        $post->comments()->create([
            'user_id' => request()->user()->id,
            'body' => request('body')
        ]);

        return back();
    }
}

```

---


### Some Light Chapter Clean Up

Vamos a realizar un poco de limpieza de código para dejarlo un poco mas ordenado y reutilizable

Creamos nuevo component reutilizable para los botones de submit llamado **/component/submit-button.blade.php**
```html
<button class="bg-blue-500 text-white uppercase font-semibold text-xs py-2 px-10 rounded-2xl hover:bg-blue-600">
    {{ $slot }}
</button>
```

Creamos nueva vista **views/posts/_add-comment-form** para agregar form de comentarios y para incluirla en la vista **posts/show**

Contenido de **_add-comment-form**
```html
@auth
    <x-panel>
        <form method="POST" action="/posts/{{ $post->slug }}/comments">
            @csrf
            <header class="flex items-center">
                <img src="https://i.pravatar.cc/60?u={{ auth()->id() }}" alt="" width="40" height="40" class="rounded-full">
                <h2 class="ml-4">Want to participate?</h2>
            </header>
            <div class="mt-6">
                <textarea name="body" class="w-full text-sm focus:outline-none focus:ring" placeholder="Quick, thing of something to say!" rows="5" required></textarea>
                @error('body')
                    <span class="text-xs text-red-500">{{ $message }}</span>
                @enderror
            </div>
            <div class="flex justify-end mt-6 border-t border-gray-200 pt-6">
                <x-submit-button>Post</x-submit-button>
            </div>
        </form>
    </x-panel>
    @else
        <p class="font-semibold">
            <a href="/register" class="hover:underline">Register</a> or <a href="/login" class="hover:underline">Login</a> to leave a comment.
        </p>
@endauth

```

Contenido actualizado de **posts/show**
```html
<x-layout>
    <section class="px-6 py-8">
        <main class="max-w-6xl mx-auto mt-10 lg:mt-20 space-y-6">
            <article class="max-w-4xl mx-auto lg:grid lg:grid-cols-12 gap-x-10">
                <div class="col-span-4 lg:text-center lg:pt-14 mb-10">
                    <img src="/images/illustration-1.png" alt="" class="rounded-xl">

                    <p class="mt-4 block text-gray-400 text-xs">
                        Published <time>{{ $post->created_at->diffForHumans() }}</time>
                    </p>

                    <div class="flex items-center lg:justify-center text-sm mt-4">
                        <img src="/images/lary-avatar.svg" alt="Lary avatar">
                        <div class="ml-3 text-left">
                            <h5 class="font-bold">
                                <a href="/?author={{ $post->author->username }}">{{ $post->author->name }}</a>
                            </h5>
                        </div>
                    </div>
                </div>

                <div class="col-span-8">
                    <div class="hidden lg:flex justify-between mb-6">
                        <a href="/"
                            class="transition-colors duration-300 relative inline-flex items-center text-lg hover:text-blue-500">
                            <svg width="22" height="22" viewBox="0 0 22 22" class="mr-2">
                                <g fill="none" fill-rule="evenodd">
                                    <path stroke="#000" stroke-opacity=".012" stroke-width=".5" d="M21 1v20.16H.84V1z">
                                    </path>
                                    <path class="fill-current"
                                        d="M13.854 7.224l-3.847 3.856 3.847 3.856-1.184 1.184-5.04-5.04 5.04-5.04z">
                                    </path>
                                </g>
                            </svg>

                            Back to Posts
                        </a>

                        <div class="space-x-2">
                            <x-category-button :category="$post->category" />
                        </div>
                    </div>

                    <h1 class="font-bold text-3xl lg:text-4xl mb-10">
                        {{ $post->title }}
                    </h1>

                    <div class="space-y-4 lg:text-lg leading-loose">
                        {!! $post->body !!}
                    </div>
                </div>
                <section class="col-span-8 col-start-5 mt-10 space-y-6">
                    @include('posts._add-comment-form')
                    @foreach ($post->comments as $comment)
                        <x-post-comment :comment="$comment" />
                    @endforeach
                </section>
            </article>
        </main>
    </section>
</x-layout>

```

---

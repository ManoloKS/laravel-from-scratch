[< Go to index](../README.md)

## Los Fundamentos

### ¿Como una ruta carga una vista?

El archivo principal de rutas se encuentra en `/routes/web.php`

Una ruta puede llamar a una vista de **blade**

```php
Route::get('/', function () {
    return view('welcome');
});

```

O de **html**
```php
Route::get('/html', function () {
    return "<h1>Hola Mundo</h1>";
});

```

Tambien puede devolver un **JSON**
```php
Route::get('/json', function () {
    return ['foo'=>'bar'];
});
```
---
### Incluir Javascript y CSS
Los archivos de .css y .js se almacenaran en la carpeta `/public/`
Crear archivo `app.css` y `app.js` en la carpeta `/public`

**app.css**
```css
body{
    background-color: navy;
    color: white;
}
```
**app.js**
```javascript
alert("Hola");
```
Desde el archivo blade se puede hacer referencia a dichos archivos de esta forma
```html
<link rel="stylesheet" href="/app.css">
<script src="/app.js"></script>
```
---

### Hacer una ruta y enlazarla
Creamos la ruta `/post` y la vista correspondiente con el nombre `post.blade.php`
```php
Route::get('/post', function () {
    return view('post');
});
```

Se hace el enlace desde la vista principal hacia la ruta creada en especifico
```html
    <h1><a href="/post">My First Post</a></h1>
```

Se agrega el código al archivo `post.blade.php`
```html
<!DOCTYPE html>

<title>My Blog</title>
<link rel="stylesheet" href="/app.css">
<script src="/app.js"></script>
<body>
    <article>
        <h1><a href="/post">My First Post</a></h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus hendrerit sapien sed commodo. Curabitur vulputate ex consectetur, tempor eros eu, laoreet libero. Vivamus vitae dapibus arcu. Duis vitae faucibus sapien, non auctor risus. Etiam finibus nunc nec dolor tincidunt, sed ullamcorper metus efficitur. Morbi vestibulum eleifend massa et convallis. Duis consectetur euismod massa a aliquet. Sed aliquet tempus mauris. Nulla facilisi. Pellentesque accumsan sagittis erat nec dapibus. Mauris erat metus, aliquam vel mauris quis, pulvinar accumsan lectus. Integer vel ante id neque vehicula rhoncus. Nullam vulputate vestibulum dui, ut ornare est. </p>
    </article>
    <a href="/">Go Back</a>
</body>

```
---
### Guardar post de blog como archivos HTML

Creamos carpeta `/resources/posts` y creamos 3 post de ejemplo en formato html `my-first-post.html`, `my-second-post.html`, `my-third-post.html` con contenido
```html
<!DOCTYPE html>

<title>My Blog</title>
<link rel="stylesheet" href="/app.css">
<script src="/app.js"></script>
<body>
    <article>
        <h1><a href="/posts/my-first-post">My First Post</a></h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus hendrerit sapien sed commodo. Curabitur vulputate ex consectetur, tempor eros eu, laoreet libero. Vivamus vitae dapibus arcu. Duis vitae faucibus sapien, non auctor risus. Etiam finibus nunc nec dolor tincidunt, sed ullamcorper metus efficitur. Morbi vestibulum eleifend massa et convallis. Duis consectetur euismod massa a aliquet. Sed aliquet tempus mauris. Nulla facilisi. Pellentesque accumsan sagittis erat nec dapibus. Mauris erat metus, aliquam vel mauris quis, pulvinar accumsan lectus. Integer vel ante id neque vehicula rhoncus. Nullam vulputate vestibulum dui, ut ornare est. </p>
    </article>
</body>

```

Se crea ruta dinamica con slug/parametro que indica cual archivo cargar
```php
Route::get('/posts/{post}', function ($slug) {
    $path = __DIR__."/../resources/posts/{$slug}.html";
    if(!file_exists($path)){
        return redirect('/');
    }
    $post = file_get_contents($path);
    return view('post',[
        'post'=>$post
    ]);
});
```

Modifcar tambien cada link desde el la vista `post` principal
```html
<h1><a href="/posts/my-first-post">My First Post</a></h1>
```
---
### Route Wildcard Constraints
Se agrega expresión regular al final de la ruta para validar lo que se ingresa a la URL
```php
Route::get('/posts/{post}', function ($slug) {
    $path = __DIR__."/../resources/posts/{$slug}.html";
    if(!file_exists($path)){
        return redirect('/');
    }
    $post = file_get_contents($path);
    return view('post',[
        'post'=>$post
    ]);
})->where('post','[A-z_\-]+');
```
---
### Use Caching for Expensive Operations
Se agrega linea de cache donde se asigna la cantidad de tiempo en segundos a almacenar una ruta en cache
```php
Route::get('/posts/{post}', function ($slug) {
    if(!file_exists($path = __DIR__."/../resources/posts/{$slug}.html")){
        return redirect('/');
    }
    $post = cache()->remember("post.{$slug}",1200, fn() => file_get_contents($path));
    return view('post',['post'=>$post]);
})->where('post','[A-z_\-]+');
```
---
### Use the filesystem class to read a directory
Creamos modelo nuevo llamada `Post.php` y se agrega el codigo necesario para utilizar la clase de `Files`
Se crea metodo `find($slug)` para buscar un post en especifico y otro metodo de `all()` para obtener todos los posts que estan en la carpetas posts
El modelo `Post.php` queda así
```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\File;

class Post
{
    public static function find($slug){
        if(!file_exists($path = resource_path("posts/{$slug}.html"))){
            throw new ModelNotFoundException();
        }
        return cache()->remember("post.{$slug}",1200, fn() => file_get_contents($path));
    }

    public static function all(){
        $files = File::files(resource_path("posts/"));
        return array_map(fn($file)=>$file->getContents(),$files);
    }
}

```

Tambien modificamos las rutas del archivo `/routes/web.php`
Ahora la variable post es llamada desde el modelo
```php
Route::get('/', function () {
    return view('posts',[
        'posts'=> Post::all()
    ]);
});

Route::get('/posts/{post}', function ($slug) {
    return view('post',[
        'post' => Post::find($slug)
    ]);
})->where('post','[A-z_\-]+');
```
Modificamos los archivos blade `posts.blade.php` y `post.blade.php`

**posts.blade.php**
```html
<!DOCTYPE html>

<title>My Blog</title>
<link rel="stylesheet" href="/app.css">
<script src="/app.js"></script>
<body>
    <?php foreach ($posts as $post) : ?>
    <article>
        <?= $post; ?>
    </article>
    <?php endforeach; ?>
</body>
```
**post.blade.php**
```html
<!DOCTYPE html>

<title>My Blog</title>
<link rel="stylesheet" href="/app.css">
<script src="/app.js"></script>
<body>
    <article>
        <?= $post; ?>
    </article>
    <a href="/">Go Back</a>
</body>
```
---
### Find a Composer Package for Post Metadata
Instalar spatie/yaml-fromt-matter para usar metadata
```console
composer require spatie/yaml-front-matter
```
Se agrega información de metadata en cada post html
```html
---
title: My First Post
slug: my-first-post
excerpt: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
date: 2021-05-21
---

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc maximus hendrerit sapien sed commodo. Curabitur vulputate ex consectetur, tempor eros eu, laoreet libero. Vivamus vitae dapibus arcu. Duis vitae faucibus sapien, non auctor risus. Etiam finibus nunc nec dolor tincidunt, sed ullamcorper metus efficitur. Morbi vestibulum eleifend massa et convallis. Duis consectetur euismod massa a aliquet. Sed aliquet tempus mauris. Nulla facilisi. Pellentesque accumsan sagittis erat nec dapibus. Mauris erat metus, aliquam vel mauris quis, pulvinar accumsan lectus. Integer vel ante id neque vehicula rhoncus. Nullam vulputate vestibulum dui, ut ornare est. </p>
```
Se modifica modelo Post.php declarando las variables del objeto y el constructor, se modica metodo all() para obtener datos de metadata de cada archivo por medio del paquete instalado y asignar cada elemento al objeto Post, se modifica tambien metodo find para obtener un objecto en especifico desde el metodo all() por medio del slug
```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\File;
use Spatie\YamlFrontMatter\YamlFrontMatter;

class Post
{
    public $title;
    public $excerpt;
    public $date;
    public $body;
    public $slug;

    public function __construct($title,$excerpt,$date,$body,$slug)
    {
        $this->title = $title;
        $this->excerpt = $excerpt;
        $this->date = $date;
        $this->body = $body;
        $this->slug = $slug;
    }

    public static function all(){
        return collect(File::files(resource_path("posts")))
        ->map(fn($file) => YamlFrontMatter::parseFile($file))
        ->map(fn($document) => new Post(
            $document->title,
            $document->excerpt,
            $document->date,
            $document->body(),
            $document->slug
        ));
    }

    public static function find($slug){
        return static::all()->firstWhere('slug',$slug);
    }
}
```
Se modifica view `posts.blade.php` para cargar todos los posts en base al array que contiene cada objeto
```html
<!DOCTYPE html>

<title>My Blog</title>
<link rel="stylesheet" href="/app.css">
<script src="/app.js"></script>
<body>
    <?php foreach ($posts as $post) : ?>
    <article>
       <h1>
            <a href="/posts/<?= $post->slug; ?>">
                <?= $post->title; ?>
            </a>
        </h1>
       <div>
           <?= $post->excerpt; ?>
       </div>
    </article>
    <?php endforeach; ?>
</body>
```

Se modifica view `post.blade.php` para mostrar información del objeto Post
```html
<!DOCTYPE html>

<title>My Blog</title>
<link rel="stylesheet" href="/app.css">
<script src="/app.js"></script>
<body>
    <article>
        <h1><?= $post->title; ?></h1>
        <div>
            <?= $post->body; ?>
        </div>
    </article>
    <a href="/">Go Back</a>
</body>
```
---
### Collection Sorting and Caching Refresher
Usar sortBy en el collection para ordenar los posts por fecha, ademas se agrega resultado de metodo all a cache
```php
    return cache()->rememberForever('post.all',function(){
        return collect(File::files(resource_path("posts")))
        ->map(fn($file) => YamlFrontMatter::parseFile($file))
        ->map(fn($document) => new Post(
            $document->title,
            $document->excerpt,
            $document->date,
            $document->body(),
            $document->slug
        ))->sortByDesc('date');
    });
```
---
[< Go to index](../README.md)
